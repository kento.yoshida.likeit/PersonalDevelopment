package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import beans.TaskInfoBeans;

public class DateAxisTaskListLogic {

	//dateAxisTaskListの全てのタスク数を返すメソッド
	public int allTaskNum(List<TaskInfoBeans> dateAxisTaskList) {
		int allTaskNum = dateAxisTaskList.size();
		return allTaskNum;
	}

	//dateAxisTaskListの消化済みタスクのリストを返すメソッド
	public List<TaskInfoBeans> dateAxisDoneTaskList(List<TaskInfoBeans> dateAxisTaskList) {
		List<TaskInfoBeans> dateAxisDoneTaskList = new ArrayList<TaskInfoBeans>();
		for (int i = 0; i < dateAxisTaskList.size(); i++) {
			if (dateAxisTaskList.get(i).getRunStatus()==2) {
				dateAxisDoneTaskList.add(dateAxisTaskList.get(i));
			}
		}
		return dateAxisDoneTaskList;
	}

	//dateAxisTaskListの消化済みタスク数を返すメソッド
	public int doneTaskNum(List<TaskInfoBeans> dateAxisDoneTaskList) {
		int doneTaskNum =dateAxisDoneTaskList.size();
		return doneTaskNum;
	}

	//全てのタスクの見積時間の合計を返すメソッド
	public String allTaskHour(List<TaskInfoBeans> dateAxisTaskList) {
		int allTaskMin = 0;
		for (int i = 0; i < dateAxisTaskList.size(); i++) {
			allTaskMin += dateAxisTaskList.get(i).getEstimatedTime();
		}
		double allTaskHour = (double)allTaskMin/60;
		String allTaskHourSTR = adjustNum(allTaskHour);
		return allTaskHourSTR;
	}

	//消化済みタスクの見積時間の合計を返すメソッド
	public String doneTaskHour(List<TaskInfoBeans> dateAxisDoneTaskList) {
		int doneTaskMin = 0;
		for (int i = 0; i < dateAxisDoneTaskList.size(); i++) {
			doneTaskMin += dateAxisDoneTaskList.get(i).getEstimatedTime();
		}
		double doneTaskHour = (double)doneTaskMin/60;
		String doneTaskHourSTR = adjustNum(doneTaskHour);
		return doneTaskHourSTR;
	}
	//残りのタスクの見積時間の合計を返すメソッド
	public String remainingTaskHour(String allTaskHourSTR,String doneTaskHourSTR) {
		double allTaskHour = Double.parseDouble(allTaskHourSTR);
		double doneTaskHour = Double.parseDouble(doneTaskHourSTR);
		double remainingTaskHour = allTaskHour-doneTaskHour;
		String remainingTaskHourSTR = adjustNum(remainingTaskHour);
		return remainingTaskHourSTR;
	}

	//小数点が0であった場合に0を消す処理
	//小数点が0でなかった場合に小数点第1位未満を四捨五入する処理
	public String adjustNum(double rawNum) {
		String adjustNum = null;
		double adjustNumD = 0;
		if(rawNum == (int) rawNum) {
			adjustNum = String.valueOf((int) rawNum);
		}else {
			adjustNum =String.format("%.1f", rawNum);
			adjustNumD = Double.parseDouble(adjustNum);
			//小数点第1位未満を四捨五入した数字の小数点が0かどうかの判定
			if(adjustNumD == (int)adjustNumD ) {
				adjustNum = String.valueOf((int) adjustNumD);
			}
		}
		return adjustNum;
	}

	//今日の日付を返すメソッド
	public String nowDate() {
		Date date=new Date();
		DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		String nowDate=dateFormat.format(date);
		return nowDate;
	}

}
