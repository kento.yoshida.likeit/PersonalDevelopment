package model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import beans.ProcessInfoBeans;

public class ProjectAxisProcessListLogic {

	//int型minTimeをmin→hourにするメソッド
	public String minToHour(int totalTaskEstimatedMinTime) {
		double totalTaskEstimatedHourTime = (double)totalTaskEstimatedMinTime/60;
		String totalTaskEstimatedHourTimeSTR = adjustNum(totalTaskEstimatedHourTime);
		return totalTaskEstimatedHourTimeSTR;
	}

	//小数点が0であった場合に0を消す処理
	//小数点が0でなかった場合に小数点第1位未満を四捨五入する処理
	public String adjustNum(double rawNum) {
		String adjustNum = null;
		double adjustNumD = 0;
		if(rawNum == (int) rawNum) {
			//小数点が完全な0かどうかの判定
			adjustNum = String.valueOf((int) rawNum);
		}else {
			//小数点第1位未満を四捨五入
			adjustNum =String.format("%.1f", rawNum);
			adjustNumD = Double.parseDouble(adjustNum);
			//小数点第1位未満を四捨五入した数字の小数点が0かどうかの判定
			if(adjustNumD == (int)adjustNumD ) {
				adjustNum = String.valueOf((int) adjustNumD);
			}
		}
		return adjustNum;
	}

	public String ymdSDF(Date rawDate) {
		SimpleDateFormat ymdSDF = new SimpleDateFormat("yyyy年MM月dd日");
		String formatedDate = null;
		if(rawDate==null) {
			formatedDate = "";
		}else {
			formatedDate = ymdSDF.format(rawDate);
		}
		return formatedDate;
	}

	public int allProcessNum(List<ProcessInfoBeans> projectAxisProcessList) {
		int allProcessNum = projectAxisProcessList.size();
		return allProcessNum;
	}

	public List<ProcessInfoBeans> projectAxisDoneProcessList(List<ProcessInfoBeans> projectAxisProcessList) {
		List<ProcessInfoBeans> projectAxisDoneProcessList = new ArrayList<ProcessInfoBeans>();
		for (int i = 0; i < projectAxisProcessList.size(); i++) {
			if (projectAxisProcessList.get(i).getRunStatus()==2) {
				projectAxisDoneProcessList.add(projectAxisProcessList.get(i));
			}
		}
		return projectAxisDoneProcessList;
	}

	public int doneProcessNum(List<ProcessInfoBeans> projectAxisDoneProcessList) {
		int doneProcessNum = projectAxisDoneProcessList.size();
		return doneProcessNum;
	}

	public String allProcessHour(List<ProcessInfoBeans> projectAxisProcessList) {
		double allProcessHour = 0;
		for (int i = 0; i < projectAxisProcessList.size(); i++) {
			allProcessHour += Double.parseDouble(projectAxisProcessList.get(i).getEstimatedTime());
		}
		String allProcessHourSTR = adjustNum(allProcessHour);
		return allProcessHourSTR;
	}

	public String doneProcessHour(List<ProcessInfoBeans> projectAxisDoneProcessList) {
		double doneProcessHour = 0;
		for (int i = 0; i < projectAxisDoneProcessList.size(); i++) {
			doneProcessHour += Double.parseDouble(projectAxisDoneProcessList.get(i).getEstimatedTime());
		}
		String doneProcessHourSTR = adjustNum(doneProcessHour);
		return doneProcessHourSTR;
	}

	public String remainingProcessHour(String allProcessHourSTR, String doneProcessHourSTR) {
		double allProcessHour = Double.parseDouble(allProcessHourSTR);
		double doneProcessHour = Double.parseDouble(doneProcessHourSTR);
		double remainingProcessHour = allProcessHour-doneProcessHour;
		String remainingProcessHourSTR = adjustNum(remainingProcessHour);
		return remainingProcessHourSTR;
	}
}
