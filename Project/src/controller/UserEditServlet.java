package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserInfoBeans;
import dao.UserInfoDAO;

/**
 * Servlet implementation class UserEditServlet
 */
@WebServlet("/UserEditServlet")
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserEditServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Integer userId= (Integer)session.getAttribute("userId");
		if(userId==null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		String userIdForUM = request.getParameter("userIdForUM");

		UserInfoDAO userInfoDAO = new UserInfoDAO();

		UserInfoBeans userInfo = null;
		if(userIdForUM != null) {
			Integer userIdForUMInteger = new Integer(userIdForUM);
			userInfo = userInfoDAO.findUserInfoByUserId(userIdForUMInteger);
			request.setAttribute("userIdForUM", userIdForUM);
		}else {
			userInfo = userInfoDAO.findUserInfoByUserId(userId);
		}
		request.setAttribute("userInfo", userInfo);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userEdit.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		Integer userId= (Integer)session.getAttribute("userId");

		String userName = request.getParameter("userName");
		String mailAddress = request.getParameter("mailAddress");
		String password =  request.getParameter("password");
		String rePassword = request.getParameter("rePassword");
		String userIdForUM = request.getParameter("userIdForUM");

		UserInfoDAO userInfoDAO = new UserInfoDAO();

		if(!password.equals(rePassword)||userName.equals("")||mailAddress.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			UserInfoBeans userInfo = null;
			if(userIdForUM != null) {
				Integer userIdForUMInteger = new Integer(userIdForUM);
				userInfo = userInfoDAO.findUserInfoByUserId(userIdForUMInteger);
				request.setAttribute("userIdForUM", userIdForUM);
			}else {
				userInfo = userInfoDAO.findUserInfoByUserId(userId);
			}

			request.setAttribute("userInfo", userInfo);

			if(userIdForUM != null) {
				request.setAttribute("userIdForUM", userIdForUM);
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userEdit.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(password.equals("")&&rePassword.equals("")) {
			if(userIdForUM != null) {
				Integer userIdForUMInteger = new Integer(userIdForUM);
				userInfoDAO.updateUserInfoNoPassword(userName,mailAddress,userIdForUMInteger);
			}else {
				userInfoDAO.updateUserInfoNoPassword(userName,mailAddress,userId);
			}
		}else {
			if(userIdForUM != null) {
				Integer userIdForUMInteger = new Integer(userIdForUM);
				userInfoDAO.updateUserInfo(userName,mailAddress,password,userIdForUMInteger);
			}else {
				userInfoDAO.updateUserInfo(userName,mailAddress,password,userId);
			}
		}
		response.sendRedirect("DateAxisTaskListServlet");
	}
}
