package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ProcessInfoBeans;
import beans.TaskInfoBeans;
import dao.ProcessInfoDAO;
import dao.ProjectInfoDAO;
import dao.TaskInfoDAO;
import model.DateAxisTaskListLogic;

/**
 * Servlet implementation class TaskSettingServlet
 */
@WebServlet("/TaskSettingServlet")
public class TaskSettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TaskSettingServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Integer userId= (Integer)session.getAttribute("userId");
		if(userId==null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String taskIdSTR = request.getParameter("taskId");
		int taskId = Integer.parseInt(taskIdSTR);
		String transitionSourceServlet = request.getParameter("transitionSourceServlet");
		String processId = request.getParameter("processId");
		String projectId = request.getParameter("projectId");

		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();
		TaskInfoBeans taskInfo = taskInfoDAO.findTaskSettingInfoByTaskId(taskIdSTR);

		ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();
		List<ProcessInfoBeans> processInfoList = processInfoDAO.findProcessInfoListForNewTaskByUserId(userId);
		request.setAttribute("processInfoList", processInfoList);

		request.setAttribute("taskInfo", taskInfo);
		request.setAttribute("taskId", taskId);
		request.setAttribute("transitionSourceServlet", transitionSourceServlet);
		request.setAttribute("projectId", projectId);
		request.setAttribute("processId", processId);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/taskSetting.jsp");
		dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String taskIdSTR = request.getParameter("taskId");
		String processIdSTR = request.getParameter("processId");
		String projectIdSTR = request.getParameter("projectId");
		String taskName = request.getParameter("taskName");
		String estimatedTimeSTR = request.getParameter("estimatedTime");
		String actionDate = request.getParameter("actionDate");
		String transitionSourceServlet = request.getParameter("transitionSourceServlet");

		if(taskName.equals("")||processIdSTR.equals("")||estimatedTimeSTR.equals("")||actionDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			HttpSession session = request.getSession();
			Integer userId= (Integer)session.getAttribute("userId");

			int taskId = Integer.parseInt(taskIdSTR);
			String processId = request.getParameter("processId");
			String projectId = request.getParameter("projectId");

			TaskInfoDAO taskInfoDAO = new TaskInfoDAO();
			TaskInfoBeans taskInfo = taskInfoDAO.findTaskSettingInfoByTaskId(taskIdSTR);

			ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();
			List<ProcessInfoBeans> processInfoList = processInfoDAO.findProcessInfoListForNewTaskByUserId(userId);
			request.setAttribute("processInfoList", processInfoList);

			request.setAttribute("taskInfo", taskInfo);
			request.setAttribute("taskId", taskId);
			request.setAttribute("transitionSourceServlet", transitionSourceServlet);
			request.setAttribute("projectId", projectId);
			request.setAttribute("processId", processId);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/taskSetting.jsp");
			dispatcher.forward(request, response);
			return;
		}
		int taskId = Integer.parseInt(taskIdSTR);
		int processId = Integer.parseInt(processIdSTR);
		int estimatedTime = Integer.parseInt(estimatedTimeSTR);

		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();

		taskInfoDAO.updateTask(taskId,processId,taskName,estimatedTime,actionDate);

		if(transitionSourceServlet.equals("DateAxisTaskListServlet")) {
			response.sendRedirect(transitionSourceServlet);

		}else if(transitionSourceServlet.equals("ProcessAxisTaskListServlet")) {
			ProcessInfoDAO processInfoDAO =new ProcessInfoDAO();
			String processName = processInfoDAO.findProcessNameByProcessId(processIdSTR);
			ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();
			String projectName = projectInfoDAO.findProjectNameByProjectId(projectIdSTR);


			//projectIdを渡してprocessAxisTaskListを取得
			List<TaskInfoBeans> processAxisTaskList = taskInfoDAO.findProcessAxisTaskList(processIdSTR);

			//ウィジェット系処理
			DateAxisTaskListLogic DATLLogic = new DateAxisTaskListLogic();

			//全てのタスク数
			int allTaskNum = DATLLogic.allTaskNum(processAxisTaskList);

			//消化済みタスクリスト
			List<TaskInfoBeans> processAxisDoneTaskList = DATLLogic.dateAxisDoneTaskList(processAxisTaskList);

			//消化済みタスク数
			int doneTaskNum = DATLLogic.doneTaskNum(processAxisDoneTaskList);

			//残りのタスク数
			int remainingTaskNum = allTaskNum - doneTaskNum;

			//全てのタスクの見積時間の合計
			String allTaskHour = DATLLogic.allTaskHour(processAxisTaskList);

			//消化済みタスクの見積時間の合計
			String doneTaskHour = DATLLogic.doneTaskHour(processAxisDoneTaskList);

			//残りのタスクの見積時間の合計
			String remainingTaskHour = DATLLogic.remainingTaskHour(allTaskHour, doneTaskHour);

			request.setAttribute("processAxisTaskList",processAxisTaskList );
			request.setAttribute("allTaskNum", allTaskNum);
			request.setAttribute("doneTaskNum", doneTaskNum);
			request.setAttribute("remainingTaskNum", remainingTaskNum);
			request.setAttribute("allTaskHour", allTaskHour);
			request.setAttribute("doneTaskHour", doneTaskHour);
			request.setAttribute("remainingTaskHour", remainingTaskHour);
			request.setAttribute("projectName", projectName);
			request.setAttribute("processName", processName);
			request.setAttribute("processId", processId);
			int projectId = Integer.parseInt(projectIdSTR);
			request.setAttribute("projectId", projectId);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/processAxisTaskList.jsp");
			dispatcher.forward(request, response);
		}
	}

}
