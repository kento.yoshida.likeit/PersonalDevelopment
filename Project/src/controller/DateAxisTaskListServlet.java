package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TaskInfoBeans;
import dao.TaskInfoDAO;
import model.DateAxisTaskListLogic;

/**
 * Servlet implementation class DateAxisTaskListServlet
 */
@WebServlet("/DateAxisTaskListServlet")
public class DateAxisTaskListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DateAxisTaskListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Integer userId= (Integer)session.getAttribute("userId");
		if(userId==null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		//ログインセッションuserIdを渡してDateAxisTaskListを取得
		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();
		List<TaskInfoBeans> dateAxisTaskList = taskInfoDAO.findDateAxisTaskList(userId);

		//ウィジェット系処理
		DateAxisTaskListLogic DATLLogic = new DateAxisTaskListLogic();

		//全てのタスク数
		int allTaskNum = DATLLogic.allTaskNum(dateAxisTaskList);

		//消化済みタスクリスト
		List<TaskInfoBeans> dateAxisDoneTaskList = DATLLogic.dateAxisDoneTaskList(dateAxisTaskList);

		//消化済みタスク数
		int doneTaskNum = DATLLogic.doneTaskNum(dateAxisDoneTaskList);

		//残りのタスク数
		int remainingTaskNum = allTaskNum - doneTaskNum;

		//全てのタスクの見積時間の合計
		String allTaskHour = DATLLogic.allTaskHour(dateAxisTaskList);

		//消化済みタスクの見積時間の合計
		String doneTaskHour = DATLLogic.doneTaskHour(dateAxisDoneTaskList);

		//残りのタスクの見積時間の合計
		String remainingTaskHour = DATLLogic.remainingTaskHour(allTaskHour, doneTaskHour);

		//今日の日付を取得
		String nowDate = DATLLogic.nowDate();

		request.setAttribute("dateAxisTaskList",dateAxisTaskList );
		request.setAttribute("allTaskNum", allTaskNum);
		request.setAttribute("doneTaskNum", doneTaskNum);
		request.setAttribute("remainingTaskNum", remainingTaskNum);
		request.setAttribute("allTaskHour", allTaskHour);
		request.setAttribute("doneTaskHour", doneTaskHour);
		request.setAttribute("remainingTaskHour", remainingTaskHour);
		request.setAttribute("screenDate", nowDate);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/dateAxisTaskList.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		Integer userId= (Integer)session.getAttribute("userId");

		//screenDate初期化
		String screenDate = null; //あらゆるscreenDateを格納するscreenDate

		//ステータス変更ボタンを押した時の処理
		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();
		//status0→status1
		String goToStatus1TaskId = request.getParameter("goToStatus1");
		screenDate = request.getParameter("screenDate"); //status0→status1 or 日付変更でのみ格納されるscreenDate
		if(goToStatus1TaskId != null) {
			taskInfoDAO.goToStatus1(goToStatus1TaskId);
		}
		//status1→status2
		String goToStatus2TaskId = request.getParameter("goToStatus2");
		if(screenDate == null) {
			screenDate = request.getParameter("screenDate"); //status1→status2でのみ格納されるscreenDate
		}
		if(goToStatus2TaskId != null) {
			taskInfoDAO.goToStatus2(goToStatus2TaskId);
		}

		//sort機能
		List<TaskInfoBeans> sortedDateAxisTaskList = null;
		String sortInfo = null;
		if(request.getParameter("statusName") != null) {
			sortInfo = request.getParameter("statusName");
			if(screenDate == null) {
				screenDate = request.getParameter("screenDate"); //sortでのみ格納されるscreenDate
			}
			if(sortInfo.equals("notStarted")) {
				int runStatus = 0;
				sortedDateAxisTaskList = taskInfoDAO.sortDATListByRunStatus(userId,runStatus,screenDate);
			}else if(sortInfo.equals("inProgress")) {
				int runStatus = 1;
				sortedDateAxisTaskList = taskInfoDAO.sortDATListByRunStatus(userId,runStatus,screenDate);
			}else if(sortInfo.equals("done")) {
				int runStatus = 2;
				sortedDateAxisTaskList = taskInfoDAO.sortDATListByRunStatus(userId,runStatus,screenDate);
			}
		}

		//JSPに必要な情報を取得

		//ログインセッションuserIdを渡してDateAxisTaskListを取得
		List<TaskInfoBeans> dateAxisTaskList = taskInfoDAO.findDateAxisTaskListByPost(userId,screenDate);

		//ウィジェット系処理
		DateAxisTaskListLogic DATLLogic = new DateAxisTaskListLogic();

		//全てのタスク数
		int allTaskNum = DATLLogic.allTaskNum(dateAxisTaskList);

		//消化済みタスクリスト
		List<TaskInfoBeans> dateAxisDoneTaskList = DATLLogic.dateAxisDoneTaskList(dateAxisTaskList);

		//消化済みタスク数
		int doneTaskNum = DATLLogic.doneTaskNum(dateAxisDoneTaskList);

		//残りのタスク数
		int remainingTaskNum = allTaskNum - doneTaskNum;

		//全てのタスクの見積時間の合計
		String allTaskHour = DATLLogic.allTaskHour(dateAxisTaskList);

		//消化済みタスクの見積時間の合計
		String doneTaskHour = DATLLogic.doneTaskHour(dateAxisDoneTaskList);

		//残りのタスクの見積時間の合計
		String remainingTaskHour = DATLLogic.remainingTaskHour(allTaskHour, doneTaskHour);

		// リクエストスコープに渡したい情報をセット
		if(sortedDateAxisTaskList != null && !sortInfo.equals("allStatus")) {
			request.setAttribute("dateAxisTaskList",sortedDateAxisTaskList );
		}else {
			request.setAttribute("dateAxisTaskList",dateAxisTaskList );
		}
		request.setAttribute("allTaskNum", allTaskNum);
		request.setAttribute("doneTaskNum", doneTaskNum);
		request.setAttribute("remainingTaskNum", remainingTaskNum);
		request.setAttribute("allTaskHour", allTaskHour);
		request.setAttribute("doneTaskHour", doneTaskHour);
		request.setAttribute("remainingTaskHour", remainingTaskHour);
		request.setAttribute("screenDate", screenDate);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/dateAxisTaskList.jsp");
		dispatcher.forward(request, response);

	}

}
