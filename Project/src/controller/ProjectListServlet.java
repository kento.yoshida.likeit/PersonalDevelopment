package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ProjectInfoBeans;
import dao.ProjectInfoDAO;

/**
 * Servlet implementation class ProjectListServlet
 */
@WebServlet("/ProjectListServlet")
public class ProjectListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjectListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
    	Integer userId= (Integer)session.getAttribute("userId");
    	if(userId==null) {
    		response.sendRedirect("LoginServlet");
    		return;
    	}

    	//ログインセッションuserIdを渡してProjectListを取得
    	ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();
    	List<ProjectInfoBeans> projectInfoList = projectInfoDAO.findProjectList(userId);

    	request.setAttribute("projectInfoList",projectInfoList);

    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/projectList.jsp");
    	dispatcher.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	request.setCharacterEncoding("UTF-8");

    	HttpSession session = request.getSession();
    	Integer userId= (Integer)session.getAttribute("userId");

    	String searchWord = request.getParameter("searchWord");

    	//ログインセッションuserIdを渡してProjectListを取得
    	ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();
    	List<ProjectInfoBeans> projectInfoList = projectInfoDAO.search(userId,searchWord);

    	request.setAttribute("projectInfoList",projectInfoList);

    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/projectList.jsp");
    	dispatcher.forward(request, response);
    }

}
