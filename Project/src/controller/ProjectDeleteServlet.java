package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ProjectInfoDAO;

/**
 * Servlet implementation class ProjectDeleteServlet
 */
@WebServlet("/ProjectDeleteServlet")
public class ProjectDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjectDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Integer userId= (Integer)session.getAttribute("userId");
		if(userId==null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String projectIdSTR = request.getParameter("projectId");
		int projectId = Integer.parseInt(projectIdSTR);

		request.setAttribute("projectId", projectId);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/projectDelete.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String projectIdSTR = request.getParameter("projectId");
		int projectId = Integer.parseInt(projectIdSTR);

		ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();

		projectInfoDAO.deleteProject(projectId);

		response.sendRedirect("ProjectListServlet");
	}

}
