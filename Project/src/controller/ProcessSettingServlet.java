package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ProcessInfoBeans;
import beans.ProjectInfoBeans;
import dao.ProcessInfoDAO;
import dao.ProjectInfoDAO;

/**
 * Servlet implementation class ProcessSettingServlet
 */
@WebServlet("/ProcessSettingServlet")
public class ProcessSettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProcessSettingServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Integer userId= (Integer)session.getAttribute("userId");
		if(userId==null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String processIdSTR = request.getParameter("processId");
		int processId = Integer.parseInt(processIdSTR);

		ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();
		ProcessInfoBeans processInfo = processInfoDAO.findProcessSettingInfoByProcessId(processId);

		ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();
		List<ProjectInfoBeans> projectInfoList = projectInfoDAO.findProjectInfoListForNewProcessByUserId(userId);

		request.setAttribute("processInfo", processInfo);
		request.setAttribute("projectInfoList", projectInfoList);
		request.setAttribute("processId", processId);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/processSetting.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String processName = request.getParameter("processName");
		String projectIdSTR = request.getParameter("projectId");
		String scheduledStartDate = request.getParameter("scheduledStartDate");
		String targetDate = request.getParameter("targetDate");
		String processIdSTR = request.getParameter("processId");

		if(processName.equals("")||projectIdSTR.equals("")||scheduledStartDate.equals("")||targetDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			HttpSession session = request.getSession();
			Integer userId= (Integer)session.getAttribute("userId");
			int processId = Integer.parseInt(processIdSTR);

			ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();
			ProcessInfoBeans processInfo = processInfoDAO.findProcessSettingInfoByProcessId(processId);

			ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();
			List<ProjectInfoBeans> projectInfoList = projectInfoDAO.findProjectInfoListForNewProcessByUserId(userId);

			request.setAttribute("processInfo", processInfo);
			request.setAttribute("projectInfoList", projectInfoList);
			request.setAttribute("processId", processId);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/processSetting.jsp");
			dispatcher.forward(request, response);
			return;
		}
		int projectId = Integer.parseInt(projectIdSTR);
		int processId = Integer.parseInt(processIdSTR);

		ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();

		processInfoDAO.updateProcess(processId,processName,projectId,scheduledStartDate,targetDate);

		request.setAttribute("projectId", projectId);

		RequestDispatcher dispatcher = request.getRequestDispatcher("ProjectAxisProcessListServlet");
		dispatcher.forward(request, response);
	}

}
