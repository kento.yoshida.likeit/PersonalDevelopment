package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ProjectInfoBeans;
import dao.ProjectInfoDAO;

/**
 * Servlet implementation class ProjectSettingServlet
 */
@WebServlet("/ProjectSettingServlet")
public class ProjectSettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjectSettingServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Integer userId= (Integer)session.getAttribute("userId");
		if(userId==null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String projectIdSTR = request.getParameter("projectId");
		int projectId = Integer.parseInt(projectIdSTR);

		ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();
		ProjectInfoBeans projectInfo = projectInfoDAO.findProjectSettingInfoByProjectId(projectId);

		request.setAttribute("projectInfo", projectInfo);
		request.setAttribute("projectId", projectId);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/projectSetting.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String projectName = request.getParameter("projectName");
		String targetDate = request.getParameter("targetDate");
		String projectIdSTR = request.getParameter("projectId");

		if(projectName.equals("")||targetDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			int projectId = Integer.parseInt(projectIdSTR);

			ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();
			ProjectInfoBeans projectInfo = projectInfoDAO.findProjectSettingInfoByProjectId(projectId);

			request.setAttribute("projectInfo", projectInfo);
			request.setAttribute("projectId", projectId);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/projectSetting.jsp");
			dispatcher.forward(request, response);
			return;
		}

		int projectId = Integer.parseInt(projectIdSTR);

		ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();
		projectInfoDAO.updateProject(projectId,projectName,targetDate);

		response.sendRedirect("ProjectListServlet");
	}

}
