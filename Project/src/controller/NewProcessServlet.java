package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ProjectInfoBeans;
import dao.ProcessInfoDAO;
import dao.ProjectInfoDAO;

/**
 * Servlet implementation class NewProcessServlet
 */
@WebServlet("/NewProcessServlet")
public class NewProcessServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewProcessServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Integer userId= (Integer)session.getAttribute("userId");
		if(userId==null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		String projectIdSTR = request.getParameter("projectId");

		ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();

		//userIdを基にuserId配下のprojectInfoリストを取得
		List<ProjectInfoBeans> projectInfoList = projectInfoDAO.findProjectInfoListForNewProcessByUserId(userId);

		//projectIdを基にprojectInfoBeansインスタンスを遷移元projectInfoとして取得
		ProjectInfoBeans transitionSourceProjectInfo = projectInfoDAO.findProjectInfoForNewProcessByProjectId(projectIdSTR);

		request.setAttribute("projectInfoList", projectInfoList);
		request.setAttribute("transitionSourceProjectInfo", transitionSourceProjectInfo);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newProcess.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String processName = request.getParameter("processName");
		String projectIdSTR = request.getParameter("projectId");
		String estimatedTimeHourSTR = request.getParameter("estimatedTimeHour");
		String scheduledStartDate = request.getParameter("scheduledStartDate");
		String targetDate = request.getParameter("targetDate");

		if(processName.equals("")||projectIdSTR.equals("")||estimatedTimeHourSTR.equals("")||scheduledStartDate.equals("")||targetDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			HttpSession session = request.getSession();
			Integer userId= (Integer)session.getAttribute("userId");

			ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();

			//userIdを基にuserId配下のprojectInfoリストを取得
			List<ProjectInfoBeans> projectInfoList = projectInfoDAO.findProjectInfoListForNewProcessByUserId(userId);

			//projectIdを基にprojectInfoBeansインスタンスを遷移元projectInfoとして取得
			ProjectInfoBeans transitionSourceProjectInfo = projectInfoDAO.findProjectInfoForNewProcessByProjectId(projectIdSTR);

			request.setAttribute("projectInfoList", projectInfoList);
			request.setAttribute("transitionSourceProjectInfo", transitionSourceProjectInfo);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newProcess.jsp");
			dispatcher.forward(request, response);
			return;
		}

		int projectId = Integer.parseInt(projectIdSTR);
		int estimatedTimeHour = Integer.parseInt(estimatedTimeHourSTR);

		//新規工程をInsert
		ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();
		processInfoDAO.newProcess(processName,projectId,estimatedTimeHour,scheduledStartDate,targetDate);

		request.setAttribute("projectId", projectId);

		RequestDispatcher dispatcher = request.getRequestDispatcher("ProjectAxisProcessListServlet");
		dispatcher.forward(request, response);


	}

}
