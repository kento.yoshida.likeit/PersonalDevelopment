package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ProcessInfoDAO;

/**
 * Servlet implementation class ProcessDeleteServlet
 */
@WebServlet("/ProcessDeleteServlet")
public class ProcessDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProcessDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Integer userId= (Integer)session.getAttribute("userId");
		if(userId==null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String processIdSTR = request.getParameter("processId");
		int processId = Integer.parseInt(processIdSTR);
		String projectId = request.getParameter("projectId");

		request.setAttribute("processId", processId);
		request.setAttribute("projectId", projectId);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/processDelete.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String processIdSTR = request.getParameter("processId");
		int processId = Integer.parseInt(processIdSTR);
		String projectId = request.getParameter("projectId");

		ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();

		processInfoDAO.deleteProcess(processId);

		request.setAttribute("projectId", projectId);

		RequestDispatcher dispatcher = request.getRequestDispatcher("ProjectAxisProcessListServlet");
		dispatcher.forward(request, response);

	}

}
