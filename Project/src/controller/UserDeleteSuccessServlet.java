package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserInfoDAO;

/**
 * Servlet implementation class UserDeleteSuccessServlet
 */
@WebServlet("/UserDeleteSuccessServlet")
public class UserDeleteSuccessServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDeleteSuccessServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		Integer userId= (Integer)session.getAttribute("userId");
		if(userId==null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String userIdForUM = request.getParameter("userIdForUM");

		UserInfoDAO userInfoDAO = new UserInfoDAO();

		userInfoDAO.userDelete(userIdForUM);

		response.sendRedirect("UserListServlet");
	}
}
