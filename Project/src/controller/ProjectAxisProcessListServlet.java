package controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ProcessInfoBeans;
import dao.ProcessInfoDAO;
import dao.ProjectInfoDAO;
import dao.TaskInfoDAO;
import model.ProjectAxisProcessListLogic;

/**
 * Servlet implementation class ProjectAxisProcessListServlet
 */
@WebServlet("/ProjectAxisProcessListServlet")
public class ProjectAxisProcessListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjectAxisProcessListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
    	Integer userId= (Integer)session.getAttribute("userId");
    	if(userId==null) {
    		response.sendRedirect("LoginServlet");
    		return;
    	}

    	String projectId = request.getParameter("projectId");

    	//userIdとprojectIdでProcessListを取得
    	ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();
    	List<ProcessInfoBeans> projectAxisProcessList = processInfoDAO.findProcessList(userId,projectId);

    	//ウィジェット系処理
    	ProjectAxisProcessListLogic PAPLLogic = new ProjectAxisProcessListLogic();

    	//projectNameを取得
    	ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();
    	String projectName = projectInfoDAO.findProjectNameByProjectId(projectId);

    	//全ての工程数
    	int allProcessNum = PAPLLogic.allProcessNum(projectAxisProcessList);

    	//消化済み工程リスト
    	List<ProcessInfoBeans> projectAxisDoneProcessList = PAPLLogic.projectAxisDoneProcessList(projectAxisProcessList);

    	//消化済み工程数
    	int doneProcessNum = PAPLLogic.doneProcessNum(projectAxisDoneProcessList);

    	//残りの工程数
    	int remainingProcessNum = allProcessNum - doneProcessNum;

    	//全ての工程の見積時間の合計
    	String allProcessHour = PAPLLogic.allProcessHour(projectAxisProcessList);

    	//消化済み工程の見積時間の合計
    	String doneProcessHour = PAPLLogic.doneProcessHour(projectAxisDoneProcessList);

    	//残りのタスクの見積時間の合計
    	String remainingProcessHour = PAPLLogic.remainingProcessHour(allProcessHour, doneProcessHour);

    	request.setAttribute("projectName", projectName);
    	request.setAttribute("projectId", projectId);
    	request.setAttribute("projectAxisProcessList",projectAxisProcessList);
    	request.setAttribute("allProcessNum", allProcessNum);
    	request.setAttribute("allProcessHour", allProcessHour);
    	request.setAttribute("doneProcessNum", doneProcessNum);
    	request.setAttribute("doneProcessHour", doneProcessHour);
    	request.setAttribute("remainingProcessNum", remainingProcessNum);
    	request.setAttribute("remainingProcessHour", remainingProcessHour);

    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/projectAxisProcessList.jsp");
    	dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		Integer userId= (Integer)session.getAttribute("userId");

		String projectId = request.getParameter("projectId");

		//ステータス変更ボタンを押した時の処理
		ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();
		//status1→status2
		String goToStatus2ProcessId = request.getParameter("goToStatus2");

		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();

		if(goToStatus2ProcessId != null) {
			boolean allTaskStatusIs2 = taskInfoDAO.checkAllTaskStatusByProcessId(goToStatus2ProcessId);
			if(allTaskStatusIs2 == true) {
				int allTaskMin = taskInfoDAO.sumTotalTaskActualTimeByProcessId(goToStatus2ProcessId);
				Timestamp finalDoneTaskTimestamp = taskInfoDAO.getFinalDoneTaskTimestampByProcessId(goToStatus2ProcessId);
				processInfoDAO.goToStatus2(goToStatus2ProcessId,allTaskMin,finalDoneTaskTimestamp);
			}
		}

		//sort機能
		List<ProcessInfoBeans> sortedProjectAxisProcessList = null;
		String sortInfo = null;
		if(request.getParameter("statusName") != null) {
			sortInfo = request.getParameter("statusName");
			if(sortInfo.equals("notStarted")) {
				int runStatus = 0;
				sortedProjectAxisProcessList = processInfoDAO.sortPAPListByRunStatus(projectId,runStatus);
			}else if(sortInfo.equals("inProgress")) {
				int runStatus = 1;
				sortedProjectAxisProcessList = processInfoDAO.sortPAPListByRunStatus(projectId,runStatus);
			}else if(sortInfo.equals("done")) {
				int runStatus = 2;
				sortedProjectAxisProcessList = processInfoDAO.sortPAPListByRunStatus(projectId,runStatus);
			}
		}
		//検索機能
		List<ProcessInfoBeans> searchedProjectAxisProcessList = null;
		String searchWord =null;
		if(request.getParameter("searchWord") !=null) {
			searchWord = request.getParameter("searchWord");
			searchedProjectAxisProcessList = processInfoDAO.searchPAPListBysearchWord(projectId,searchWord);
		}

    	//userIdとprojectIdでProcessListを取得
    	List<ProcessInfoBeans> projectAxisProcessList = processInfoDAO.findProcessList(userId,projectId);

    	//ウィジェット系処理
    	ProjectAxisProcessListLogic PAPLLogic = new ProjectAxisProcessListLogic();

    	//projectNameを取得
    	ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();
    	String projectName = projectInfoDAO.findProjectNameByProjectId(projectId);

    	//全ての工程数
    	int allProcessNum = PAPLLogic.allProcessNum(projectAxisProcessList);

    	//消化済み工程リスト
    	List<ProcessInfoBeans> projectAxisDoneProcessList = PAPLLogic.projectAxisDoneProcessList(projectAxisProcessList);

    	//消化済み工程数
    	int doneProcessNum = PAPLLogic.doneProcessNum(projectAxisDoneProcessList);

    	//残りの工程数
    	int remainingProcessNum = allProcessNum - doneProcessNum;

    	//全ての工程の見積時間の合計
    	String allProcessHour = PAPLLogic.allProcessHour(projectAxisProcessList);

    	//消化済み工程の見積時間の合計
    	String doneProcessHour = PAPLLogic.doneProcessHour(projectAxisDoneProcessList);

    	//残りのタスクの見積時間の合計
    	String remainingProcessHour = PAPLLogic.remainingProcessHour(allProcessHour, doneProcessHour);

    	if(sortedProjectAxisProcessList != null && !sortInfo.equals("allStatus")) {
    		request.setAttribute("projectAxisProcessList",sortedProjectAxisProcessList );
    	}else if(searchedProjectAxisProcessList != null){
    		request.setAttribute("projectAxisProcessList", searchedProjectAxisProcessList);
    	}else {
    		request.setAttribute("projectAxisProcessList",projectAxisProcessList);
    	}
    	request.setAttribute("projectName", projectName);
    	request.setAttribute("projectId", projectId);
    	request.setAttribute("allProcessNum", allProcessNum);
    	request.setAttribute("allProcessHour", allProcessHour);
    	request.setAttribute("doneProcessNum", doneProcessNum);
    	request.setAttribute("doneProcessHour", doneProcessHour);
    	request.setAttribute("remainingProcessNum", remainingProcessNum);
    	request.setAttribute("remainingProcessHour", remainingProcessHour);

    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/projectAxisProcessList.jsp");
    	dispatcher.forward(request, response);
	}

}
