package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TaskInfoBeans;
import dao.ProcessInfoDAO;
import dao.ProjectInfoDAO;
import dao.TaskInfoDAO;
import model.DateAxisTaskListLogic;

/**
 * Servlet implementation class ProcessAxisTaskListServlet
 */
@WebServlet("/ProcessAxisTaskListServlet")
public class ProcessAxisTaskListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProcessAxisTaskListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Integer userId= (Integer)session.getAttribute("userId");
		if(userId==null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		String processId = request.getParameter("processId");
		String projectId = request.getParameter("projectId");

		ProcessInfoDAO processInfoDAO =new ProcessInfoDAO();
		String processName = processInfoDAO.findProcessNameByProcessId(processId);

		ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();
		String projectName = projectInfoDAO.findProjectNameByProjectId(projectId);


		//projectIdを渡してprocessAxisTaskListを取得
		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();
		List<TaskInfoBeans> processAxisTaskList = taskInfoDAO.findProcessAxisTaskList(processId);

		//ウィジェット系処理
		DateAxisTaskListLogic DATLLogic = new DateAxisTaskListLogic();

		//全てのタスク数
		int allTaskNum = DATLLogic.allTaskNum(processAxisTaskList);

		//消化済みタスクリスト
		List<TaskInfoBeans> processAxisDoneTaskList = DATLLogic.dateAxisDoneTaskList(processAxisTaskList);

		//消化済みタスク数
		int doneTaskNum = DATLLogic.doneTaskNum(processAxisDoneTaskList);

		//残りのタスク数
		int remainingTaskNum = allTaskNum - doneTaskNum;

		//全てのタスクの見積時間の合計
		String allTaskHour = DATLLogic.allTaskHour(processAxisTaskList);

		//消化済みタスクの見積時間の合計
		String doneTaskHour = DATLLogic.doneTaskHour(processAxisDoneTaskList);

		//残りのタスクの見積時間の合計
		String remainingTaskHour = DATLLogic.remainingTaskHour(allTaskHour, doneTaskHour);

		request.setAttribute("processAxisTaskList",processAxisTaskList );
		request.setAttribute("allTaskNum", allTaskNum);
		request.setAttribute("doneTaskNum", doneTaskNum);
		request.setAttribute("remainingTaskNum", remainingTaskNum);
		request.setAttribute("allTaskHour", allTaskHour);
		request.setAttribute("doneTaskHour", doneTaskHour);
		request.setAttribute("remainingTaskHour", remainingTaskHour);
		request.setAttribute("projectName", projectName);
		request.setAttribute("processName", processName);
		request.setAttribute("processId", processId);
		request.setAttribute("projectId", projectId);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/processAxisTaskList.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//全てのdoPostでprocessIdが送られてくる
		String processId = request.getParameter("processId");
		String projectId = request.getParameter("projectId");
		ProcessInfoDAO processInfoDAO =new ProcessInfoDAO();
		String processName = processInfoDAO.findProcessNameByProcessId(processId);
		ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();
		String projectName = projectInfoDAO.findProjectNameByProjectId(projectId);


		//ステータス変更ボタンを押した時の処理
		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();
		//status0→status1
		String goToStatus1TaskId = request.getParameter("goToStatus1");
		if(goToStatus1TaskId != null) {
			taskInfoDAO.goToStatus1(goToStatus1TaskId);
		}
		//status1→status2
		String goToStatus2TaskId = request.getParameter("goToStatus2");
		if(goToStatus2TaskId != null) {
			taskInfoDAO.goToStatus2(goToStatus2TaskId);
		}

		//sort機能
		List<TaskInfoBeans> sortedProcessAxisTaskList = null;
		String sortInfo = null;
		if(request.getParameter("statusName") != null) {
			sortInfo = request.getParameter("statusName");
			if(sortInfo.equals("notStarted")) {
				int runStatus = 0;
				sortedProcessAxisTaskList = taskInfoDAO.sortPATListByRunStatus(processId,runStatus);
			}else if(sortInfo.equals("inProgress")) {
				int runStatus = 1;
				sortedProcessAxisTaskList = taskInfoDAO.sortPATListByRunStatus(processId,runStatus);
			}else if(sortInfo.equals("done")) {
				int runStatus = 2;
				sortedProcessAxisTaskList = taskInfoDAO.sortPATListByRunStatus(processId,runStatus);
			}
		}

		//検索機能
		List<TaskInfoBeans> searchedProcessAxisTaskList = null;
		String searchWord =null;
		if(request.getParameter("searchWord") !=null) {
			searchWord = request.getParameter("searchWord");
			searchedProcessAxisTaskList = taskInfoDAO.searchPATListBysearchWord(processId,searchWord);
		}


		//JSPに必要な情報を取得

		//processIdを渡してProcessAxisTaskListを取得
		List<TaskInfoBeans> processAxisTaskList = taskInfoDAO.findProcessAxisTaskList(processId);

		//ウィジェット系処理
		DateAxisTaskListLogic DATLLogic = new DateAxisTaskListLogic();

		//全てのタスク数
		int allTaskNum = DATLLogic.allTaskNum(processAxisTaskList);

		//消化済みタスクリスト
		List<TaskInfoBeans> processAxisDoneTaskList = DATLLogic.dateAxisDoneTaskList(processAxisTaskList);

		//消化済みタスク数
		int doneTaskNum = DATLLogic.doneTaskNum(processAxisDoneTaskList);

		//残りのタスク数
		int remainingTaskNum = allTaskNum - doneTaskNum;

		//全てのタスクの見積時間の合計
		String allTaskHour = DATLLogic.allTaskHour(processAxisTaskList);

		//消化済みタスクの見積時間の合計
		String doneTaskHour = DATLLogic.doneTaskHour(processAxisDoneTaskList);

		//残りのタスクの見積時間の合計
		String remainingTaskHour = DATLLogic.remainingTaskHour(allTaskHour, doneTaskHour);

		if(sortedProcessAxisTaskList != null && !sortInfo.equals("allStatus")) {
			request.setAttribute("processAxisTaskList",sortedProcessAxisTaskList );
		}else if(searchedProcessAxisTaskList != null) {
			request.setAttribute("processAxisTaskList", searchedProcessAxisTaskList);
		}else {
			request.setAttribute("processAxisTaskList",processAxisTaskList );
		}
		request.setAttribute("allTaskNum", allTaskNum);
		request.setAttribute("doneTaskNum", doneTaskNum);
		request.setAttribute("remainingTaskNum", remainingTaskNum);
		request.setAttribute("allTaskHour", allTaskHour);
		request.setAttribute("doneTaskHour", doneTaskHour);
		request.setAttribute("remainingTaskHour", remainingTaskHour);
		request.setAttribute("processId", processId);
		request.setAttribute("projectName", projectName);
		request.setAttribute("processName", processName);
		request.setAttribute("projectId", projectId);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/processAxisTaskList.jsp");
		dispatcher.forward(request, response);
	}

}
