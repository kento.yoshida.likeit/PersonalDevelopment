package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ProcessInfoBeans;
import beans.TaskInfoBeans;
import dao.ProcessInfoDAO;
import dao.ProjectInfoDAO;
import dao.TaskInfoDAO;
import model.DateAxisTaskListLogic;

/**
 * Servlet implementation class NewTaskServlet
 */
@WebServlet("/NewTaskServlet")
public class NewTaskServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewTaskServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Integer userId= (Integer)session.getAttribute("userId");
		if(userId==null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String screenDate = request.getParameter("screenDate");
		String transitionSourceServlet = request.getParameter("transitionSourceServlet");


		if(transitionSourceServlet.equals("DateAxisTaskListServlet")) {
			request.setAttribute("screenDate", screenDate);
		}

		ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();

		//Process軸タスクリスト画面からタスクを登録する際に、ProcessIdを受け取る
		String processId = request.getParameter("processId");

		ProcessInfoBeans transitionSourceProcessInfo = null;

		//userIdを基にuserId配下のprocessInfoリストを取得
		List<ProcessInfoBeans> processInfoList = processInfoDAO.findProcessInfoListForNewTaskByUserId(userId);
		request.setAttribute("processInfoList", processInfoList);

		if(transitionSourceServlet.equals("ProcessAxisTaskListServlet")) {
			//processIdを基にprocessInfoBeansインスタンスを遷移元processInfoとして取得
			transitionSourceProcessInfo = processInfoDAO.findProcessInfoForNewTaskByProcessId(processId);
			request.setAttribute("transitionSourceProcessInfo", transitionSourceProcessInfo);
		}
		request.setAttribute("transitionSourceServlet", transitionSourceServlet);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newTask.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String taskName = request.getParameter("taskName");
		String processIdSTR = request.getParameter("processId");
		String estimatedTimeSTR = request.getParameter("estimatedTime");
		String actionDate = request.getParameter("actionDate");
		String transitionSourceServlet = request.getParameter("transitionSourceServlet");

		if(taskName.equals("")||processIdSTR.equals("")||estimatedTimeSTR.equals("")||actionDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			HttpSession session = request.getSession();
			Integer userId= (Integer)session.getAttribute("userId");

			String screenDate = request.getParameter("screenDate");

			if(transitionSourceServlet.equals("DateAxisTaskListServlet")) {
				request.setAttribute("screenDate", screenDate);
			}

			ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();

			//Process軸タスクリスト画面からタスクを登録する際に、ProcessIdを受け取る
			String processId = request.getParameter("processId");
			ProcessInfoBeans transitionSourceProcessInfo = null;
			//userIdを基にuserId配下のprocessInfoリストを取得
			List<ProcessInfoBeans> processInfoList = processInfoDAO.findProcessInfoListForNewTaskByUserId(userId);
			request.setAttribute("processInfoList", processInfoList);
			if(transitionSourceServlet.equals("ProcessAxisTaskListServlet")) {
				//processIdを基にprocessInfoBeansインスタンスを遷移元processInfoとして取得
				transitionSourceProcessInfo = processInfoDAO.findProcessInfoForNewTaskByProcessId(processId);
				request.setAttribute("transitionSourceProcessInfo", transitionSourceProcessInfo);
			}
			request.setAttribute("transitionSourceServlet", transitionSourceServlet);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newTask.jsp");
			dispatcher.forward(request, response);
			return;
		}
		int processId = Integer.parseInt(processIdSTR);
		int estimatedTime = Integer.parseInt(estimatedTimeSTR);

		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();

		taskInfoDAO.newTask(taskName,processId,estimatedTime,actionDate);

		ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();
		ProcessInfoBeans transitionSourceProcessInfo = null;

		if(transitionSourceServlet.equals("DateAxisTaskListServlet")) {
			response.sendRedirect(transitionSourceServlet);

		}else if(transitionSourceServlet.equals("ProcessAxisTaskListServlet")) {

			request.setAttribute("processId", processId);
			transitionSourceProcessInfo = processInfoDAO.findProcessInfoForNewTaskByProcessId(processIdSTR);
			int projectId = transitionSourceProcessInfo.getProjectId();
			String processName = processInfoDAO.findProcessNameByProcessId(processIdSTR);
			ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();

			String projectIdSTR = String.valueOf(projectId);
			String projectName = projectInfoDAO.findProjectNameByProjectId(projectIdSTR);


			//projectIdを渡してprocessAxisTaskListを取得
			List<TaskInfoBeans> processAxisTaskList = taskInfoDAO.findProcessAxisTaskList(processIdSTR);

			//ウィジェット系処理
			DateAxisTaskListLogic DATLLogic = new DateAxisTaskListLogic();

			//全てのタスク数
			int allTaskNum = DATLLogic.allTaskNum(processAxisTaskList);

			//消化済みタスクリスト
			List<TaskInfoBeans> processAxisDoneTaskList = DATLLogic.dateAxisDoneTaskList(processAxisTaskList);

			//消化済みタスク数
			int doneTaskNum = DATLLogic.doneTaskNum(processAxisDoneTaskList);

			//残りのタスク数
			int remainingTaskNum = allTaskNum - doneTaskNum;

			//全てのタスクの見積時間の合計
			String allTaskHour = DATLLogic.allTaskHour(processAxisTaskList);

			//消化済みタスクの見積時間の合計
			String doneTaskHour = DATLLogic.doneTaskHour(processAxisDoneTaskList);

			//残りのタスクの見積時間の合計
			String remainingTaskHour = DATLLogic.remainingTaskHour(allTaskHour, doneTaskHour);

			request.setAttribute("processAxisTaskList",processAxisTaskList );
			request.setAttribute("allTaskNum", allTaskNum);
			request.setAttribute("doneTaskNum", doneTaskNum);
			request.setAttribute("remainingTaskNum", remainingTaskNum);
			request.setAttribute("allTaskHour", allTaskHour);
			request.setAttribute("doneTaskHour", doneTaskHour);
			request.setAttribute("remainingTaskHour", remainingTaskHour);
			request.setAttribute("projectName", projectName);
			request.setAttribute("processName", processName);
			request.setAttribute("processId", processId);
			request.setAttribute("projectId", projectId);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/processAxisTaskList.jsp");
			dispatcher.forward(request, response);

		}

	}

}
