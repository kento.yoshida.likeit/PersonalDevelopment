package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import base.DBManager;
import beans.ProcessInfoBeans;
import model.ProjectAxisProcessListLogic;

public class ProcessInfoDAO {

	public int processQuantity(int projectId) {

		Connection con = null;
		PreparedStatement pStmt = null;
		int processQuantity = 0;

		try {
			con = DBManager.getConnection();

			// projectIdのprocessQuantityを取得するSELECT文
			String selectSQL = "SELECT COUNT(id) from process_info WHERE project_id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, projectId);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				processQuantity = rs.getInt("COUNT(id)");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return processQuantity;
	}

	public List<ProcessInfoBeans> findProcessList(Integer userId, String projectId) {

		Connection con = null;
		PreparedStatement pStmt = null;
		List<ProcessInfoBeans> projectAxisProcessList = new ArrayList<ProcessInfoBeans>();
		int intUserId = userId.intValue();
		int intProjectId = Integer.parseInt(projectId);
		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();
		ProjectAxisProcessListLogic PAPLLogic = new ProjectAxisProcessListLogic();

		try {
			con = DBManager.getConnection();

			// userIdとprojectIdで絞り込んだprocessListを取得するSELECT文
			String selectSQL = "SELECT * FROM process_info cefo INNER JOIN project_info jefo ON cefo.project_id = "
					+ "jefo.id WHERE cefo.project_id = ? AND jefo.user_id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, intProjectId);
			pStmt.setInt(2, intUserId);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {

				int processId = rs.getInt("id");
				String processName = rs.getString("process_name");
				int getIntProjectId = rs.getInt("project_id");

				//工程にぶら下がっているtask数をtaskInfoDAOから取得
				int totalTaskNum = taskInfoDAO.countTotalTaskNum(processId);

				//毎回processEstimatedTimeをtaskInfoから更新
				int totalTaskEstimatedMinTime = taskInfoDAO.sumTotalTaskEstimatedTimeByProcessId(processId);

				//int totalTaskEstimatedMinTimeをprocess_infoへupdate
				updateEstimatedTimeAsMin(totalTaskEstimatedMinTime,processId);

				//totalTaskEstimatedMinTimeを綺麗に整形
				//String totalTaskEstimatedHourTimeSTRをProcessInfoBeansのestimatedTimeフィールドに詰める
				String totalTaskEstimatedHourTimeSTR = PAPLLogic.minToHour(totalTaskEstimatedMinTime);

				int actualMinTime = rs.getInt("actual_time");

				//String actualHourTimeSTRをProcessInfoBeansのactualTimeフィールドに詰める
				String actualHourTimeSTR = PAPLLogic.minToHour(actualMinTime);

				Date scheduledStartDate = rs.getDate("scheduled_start_date");
				String formatedScheduledStartDate = PAPLLogic.ymdSDF(scheduledStartDate);

				Date startDate = rs.getDate("start_date");
				String formatedStartDate = PAPLLogic.ymdSDF(startDate);

				Date targetDate = rs.getDate("target_date");
				String formatedTargetDate = PAPLLogic.ymdSDF(targetDate);

				Date completionDate = rs.getDate("completion_date");
				String formatedCompletionDate = PAPLLogic.ymdSDF(completionDate);

				int runStatus = rs.getInt("run_status");
				String projectName = rs.getString("project_name");

				ProcessInfoBeans processInfo = new ProcessInfoBeans(processId, processName, getIntProjectId,
						totalTaskEstimatedHourTimeSTR, actualHourTimeSTR,formatedScheduledStartDate, formatedStartDate,
						formatedTargetDate,formatedCompletionDate , runStatus, projectName, totalTaskNum);

				projectAxisProcessList.add(processInfo);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return projectAxisProcessList;
	}

	public int statusCheckGoTo1(int taskId) {

		Connection con = null;
		PreparedStatement pStmt = null;
		int processRunStatus = 0;

		try {
			con = DBManager.getConnection();

			// taskIdのprocessのrunStatusを取得するSELECT文
			String selectSQL = "SELECT cefo.run_status AS process_run_status FROM process_info cefo INNER JOIN "
					+ "task_info tafo ON cefo.id = tafo.process_id WHERE tafo.id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, taskId);

			ResultSet rs = pStmt.executeQuery();

			rs.next();
			processRunStatus = rs.getInt("process_run_status");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return processRunStatus;
	}

	public void goToStasus1(int taskId) {

		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBManager.getConnection();

			// taskIdのprocessのrunStatusを0→1にするUPDATE文
			String selectSQL = "UPDATE process_info cefo INNER JOIN task_info tafo ON cefo.id = tafo.process_id "
					+ "SET cefo.start_date = now(),cefo.run_status = 1 WHERE tafo.id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, taskId);

			pStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void updateEstimatedTimeAsMin(int totalTaskEstimatedMinTime,int processId) {

		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBManager.getConnection();

			// process_infoのestimatedTimeをtotalTaskEstimatedMinTimeにupdateするSQL
			String updateSQL = "UPDATE process_info SET estimated_time = ? WHERE id = ?";

			pStmt = con.prepareStatement(updateSQL);

			pStmt.setInt(1, totalTaskEstimatedMinTime);
			pStmt.setInt(2, processId);

			pStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

	}

	public void goToStatus2(String goToStatus2ProcessId, int allTaskMin, Timestamp finalDoneTaskTimestamp) {
		Connection con = null;
		PreparedStatement pStmt = null;
		int processId = Integer.parseInt(goToStatus2ProcessId);

		try {
			con = DBManager.getConnection();

			//run_status→2 actual_time→allTaskMin completion_date→finalDoneTaskTimestamp
			String updateSQL = "UPDATE process_info SET run_status = 2,actual_time = ?,"
					+ "completion_date = ? WHERE id = ?";

			pStmt = con.prepareStatement(updateSQL);

			pStmt.setInt(1, allTaskMin);
			pStmt.setTimestamp(2, finalDoneTaskTimestamp);
			pStmt.setInt(3, processId);

			pStmt.execute();

		}catch(SQLException e){
			e.printStackTrace();
			return;

		}finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}

	}

	public List<ProcessInfoBeans> sortPAPListByRunStatus(String projectId, int sentRunStatus) {

		Connection con = null;
		PreparedStatement pStmt = null;
		List<ProcessInfoBeans> sortPAPList = new ArrayList<ProcessInfoBeans>();
		int intProjectId = Integer.parseInt(projectId);
		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();
		ProjectAxisProcessListLogic PAPLLogic = new ProjectAxisProcessListLogic();

		try {
			con = DBManager.getConnection();

			// userIdとprojectIdで絞り込んだprocessListを取得するSELECT文
			String selectSQL = "SELECT * FROM process_info cefo INNER JOIN project_info jefo ON cefo.project_id = "
					+ "jefo.id WHERE cefo.project_id = ? AND cefo.run_status = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, intProjectId);
			pStmt.setInt(2, sentRunStatus);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {

				int processId = rs.getInt("id");
				String processName = rs.getString("process_name");
				int getIntProjectId = rs.getInt("project_id");

				//工程にぶら下がっているtask数をtaskInfoDAOから取得
				int totalTaskNum = taskInfoDAO.countTotalTaskNum(processId);

				//毎回processEstimatedTimeをtaskInfoから更新
				int totalTaskEstimatedMinTime = taskInfoDAO.sumTotalTaskEstimatedTimeByProcessId(processId);

				//int totalTaskEstimatedMinTimeをprocess_infoへupdate
				updateEstimatedTimeAsMin(totalTaskEstimatedMinTime,processId);

				//totalTaskEstimatedMinTimeを綺麗に整形
				//String totalTaskEstimatedHourTimeSTRをProcessInfoBeansのestimatedTimeフィールドに詰める
				String totalTaskEstimatedHourTimeSTR = PAPLLogic.minToHour(totalTaskEstimatedMinTime);

				int actualMinTime = rs.getInt("actual_time");

				//String actualHourTimeSTRをProcessInfoBeansのactualTimeフィールドに詰める
				String actualHourTimeSTR = PAPLLogic.minToHour(actualMinTime);

				Date scheduledStartDate = rs.getDate("scheduled_start_date");
				String formatedScheduledStartDate = PAPLLogic.ymdSDF(scheduledStartDate);

				Date startDate = rs.getDate("start_date");
				String formatedStartDate = PAPLLogic.ymdSDF(startDate);

				Date targetDate = rs.getDate("target_date");
				String formatedTargetDate = PAPLLogic.ymdSDF(targetDate);

				Date completionDate = rs.getDate("completion_date");
				String formatedCompletionDate = PAPLLogic.ymdSDF(completionDate);

				int runStatus = rs.getInt("run_status");
				String projectName = rs.getString("project_name");

				ProcessInfoBeans processInfo = new ProcessInfoBeans(processId, processName, getIntProjectId,
						totalTaskEstimatedHourTimeSTR, actualHourTimeSTR,formatedScheduledStartDate, formatedStartDate,
						formatedTargetDate,formatedCompletionDate , runStatus, projectName, totalTaskNum);

				sortPAPList.add(processInfo);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return sortPAPList;
	}

	public List<ProcessInfoBeans> searchPAPListBysearchWord(String projectId, String searchWord) {

		Connection con = null;
		PreparedStatement pStmt = null;
		List<ProcessInfoBeans> searchPAPListBysearchWord = new ArrayList<ProcessInfoBeans>();
		int intProjectId = Integer.parseInt(projectId);
		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();
		ProjectAxisProcessListLogic PAPLLogic = new ProjectAxisProcessListLogic();

		try {
			con = DBManager.getConnection();

			// searchWordとprojectIdで絞り込んだprocessListを取得するSELECT文
			String selectSQL = "SELECT * FROM process_info cefo INNER JOIN project_info jefo ON cefo.project_id "
					+ "= jefo.id WHERE cefo.project_id = ? AND cefo.process_name LIKE ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, intProjectId);
			pStmt.setString(2, "%" + searchWord + "%");

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int processId = rs.getInt("id");
				String processName = rs.getString("process_name");
				int getIntProjectId = rs.getInt("project_id");

				//工程にぶら下がっているtask数をtaskInfoDAOから取得
				int totalTaskNum = taskInfoDAO.countTotalTaskNum(processId);

				//毎回processEstimatedTimeをtaskInfoから更新
				int totalTaskEstimatedMinTime = taskInfoDAO.sumTotalTaskEstimatedTimeByProcessId(processId);

				//int totalTaskEstimatedMinTimeをprocess_infoへupdate
				updateEstimatedTimeAsMin(totalTaskEstimatedMinTime,processId);

				//totalTaskEstimatedMinTimeを綺麗に整形
				//String totalTaskEstimatedHourTimeSTRをProcessInfoBeansのestimatedTimeフィールドに詰める
				String totalTaskEstimatedHourTimeSTR = PAPLLogic.minToHour(totalTaskEstimatedMinTime);

				int actualMinTime = rs.getInt("actual_time");

				//String actualHourTimeSTRをProcessInfoBeansのactualTimeフィールドに詰める
				String actualHourTimeSTR = PAPLLogic.minToHour(actualMinTime);

				Date scheduledStartDate = rs.getDate("scheduled_start_date");
				String formatedScheduledStartDate = PAPLLogic.ymdSDF(scheduledStartDate);

				Date startDate = rs.getDate("start_date");
				String formatedStartDate = PAPLLogic.ymdSDF(startDate);

				Date targetDate = rs.getDate("target_date");
				String formatedTargetDate = PAPLLogic.ymdSDF(targetDate);

				Date completionDate = rs.getDate("completion_date");
				String formatedCompletionDate = PAPLLogic.ymdSDF(completionDate);

				int runStatus = rs.getInt("run_status");
				String projectName = rs.getString("project_name");

				ProcessInfoBeans processInfo = new ProcessInfoBeans(processId, processName, getIntProjectId,
						totalTaskEstimatedHourTimeSTR, actualHourTimeSTR,formatedScheduledStartDate, formatedStartDate,
						formatedTargetDate,formatedCompletionDate , runStatus, projectName, totalTaskNum);

				searchPAPListBysearchWord.add(processInfo);


			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return searchPAPListBysearchWord;
	}

	public ArrayList<Integer> findAllProcessIdForUserDeleteByProjectId(ArrayList<Integer> projectIdList) {

		ArrayList<Integer> processIdList = new ArrayList<Integer>();
		Connection con = null;
		PreparedStatement pStmt = null;

		for(Integer projectID:projectIdList) {
			try {
				int projectId = projectID.intValue();

				con = DBManager.getConnection();

				// projectIdのprojectNameを取得するSELECT文
				String selectSQL = "SELECT id FROM process_info WHERE project_id = ?";

				pStmt = con.prepareStatement(selectSQL);

				pStmt.setInt(1,projectId);

				ResultSet rs = pStmt.executeQuery();

				while (rs.next()) {
					int processId = rs.getInt("id");
					Integer processID = new Integer(processId);

					processIdList.add(processID);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return processIdList;
	}

	public void processDeleteByProjectId(ArrayList<Integer> projectIdList) {

		Connection con = null;
		PreparedStatement pStmt = null;

		for(Integer projectID:projectIdList)
			try {
				int projectId = projectID.intValue();

				con = DBManager.getConnection();

				// projectIdのprojectNameを取得するSELECT文
				String deleteSQL = "DELETE FROM process_info WHERE project_id = ?";

				pStmt = con.prepareStatement(deleteSQL);

				pStmt.setInt(1,projectId);

				pStmt.execute();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
	}

	public ProcessInfoBeans findProcessInfoForNewTaskByProcessId(String processIdSTR) {

		Connection con = null;
		PreparedStatement pStmt = null;
		int processId = Integer.parseInt(processIdSTR);
		ProcessInfoBeans transitionSourceProcessInfo = null;

		try {
			con = DBManager.getConnection();

			// taskIdのprocessのrunStatusを取得するSELECT文
			String selectSQL = "SELECT cefo.id,process_name,project_name,cefo.project_id FROM process_info cefo INNER "
					+ "JOIN project_info jefo ON cefo.project_id = jefo.id WHERE cefo.id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, processId);

			ResultSet rs = pStmt.executeQuery();

			rs.next();
			int processID = rs.getInt("id");
			int projectId = rs.getInt("project_id");
			String processName = rs.getString("process_name");
			String projectName = rs.getString("project_name");

			transitionSourceProcessInfo = new ProcessInfoBeans(processID,projectId,processName,projectName);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return transitionSourceProcessInfo;
	}

	public List<ProcessInfoBeans> findProcessInfoListForNewTaskByUserId(Integer userId) {

		Connection con = null;
		PreparedStatement pStmt = null;
		int intUserId = userId.intValue();
		List<ProcessInfoBeans> processInfoList = new ArrayList<ProcessInfoBeans>();

		try {
			con = DBManager.getConnection();

			// taskIdのprocessのrunStatusを取得するSELECT文
			String selectSQL = "SELECT cefo.id,process_name,project_name FROM process_info cefo INNER JOIN project_info "
					+ "jefo ON cefo.project_id = jefo.id INNER JOIN user_info "
					+ "sefo ON jefo.user_id = sefo.id WHERE sefo.id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, intUserId);

			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				int processID = rs.getInt("id");
				String processName = rs.getString("process_name");
				String projectName = rs.getString("project_name");

				ProcessInfoBeans processInfo = new ProcessInfoBeans(processID,processName,projectName);

				processInfoList.add(processInfo);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return processInfoList;
	}

	public void newProcess(String processName, int projectId, int estimatedTimeHour, String scheduledStartDate,
			String targetDate) {
		Connection con = null;
		PreparedStatement pStmt = null;
		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();
		int estimatedTime = estimatedTimeHour*60;
		try {
			con = DBManager.getConnection();

			// タスク登録SQL
			String insertSQL = "INSERT INTO process_info(process_name,project_id,estimated_time,scheduled_start_date,"
					+ "target_date,run_status) VALUES(?,?,?,?,?,0);";

			pStmt = con.prepareStatement(insertSQL);
			pStmt.setString(1,processName);
			pStmt.setInt(2, projectId);
			pStmt.setInt(3, estimatedTime);
			pStmt.setString(4, scheduledStartDate);
			pStmt.setString(5, targetDate);

			pStmt.execute();

			int latestProcessId = findLatestProcessId();

			taskInfoDAO.newTask("工程見積時間用タスク",latestProcessId, estimatedTime,scheduledStartDate );

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public int findLatestProcessId() {
		Connection con = null;
		PreparedStatement pStmt = null;
		int latestProcessId = 0;

		try {
			con = DBManager.getConnection();

			// projectIdのprocessQuantityを取得するSELECT文
			String selectSQL = "SELECT id FROM process_info order by id desc";

			pStmt = con.prepareStatement(selectSQL);

			ResultSet rs = pStmt.executeQuery();

			rs.next();
			latestProcessId = rs.getInt("id");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return latestProcessId;
	}

	public ProcessInfoBeans findProcessSettingInfoByProcessId(int processId) {
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBManager.getConnection();

			//ProcessInfoを取得するSQL
			String selectSQL = "SELECT process_name,project_id,scheduled_start_date,cefo.target_date,project_name "
					+ "from process_info cefo inner join project_info jefo on cefo.project_id = jefo.id  WHERE cefo.id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, processId);

			ResultSet rs = pStmt.executeQuery();

			ProcessInfoBeans processInfo = new ProcessInfoBeans();

			if(rs.next()) {
				String processName = rs.getString("process_name");
				int projectId = rs.getInt("project_id");
				String scheduledStartDate = rs.getString("scheduled_start_date");
				String targetDate = rs.getString("target_date");
				String projectName = rs.getString("project_name");

				processInfo.setProcessName(processName);
				processInfo.setProjectId(projectId);
				processInfo.setScheduledStartDate(scheduledStartDate);
				processInfo.setTargetDate(targetDate);
				processInfo.setProjectName(projectName);
			}
			return processInfo;

		}catch(SQLException e){
			e.printStackTrace();
			return null;

		}finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
	}

	public void updateProcess(int processId, String processName, int projectId, String scheduledStartDate,
			String targetDate) {
		Connection con = null;
		PreparedStatement pStmt = null;
		try {
			con = DBManager.getConnection();

			// 工程updateSQL
			String updateSQL = "UPDATE process_info SET process_name = ?,project_id = ?,scheduled_start_date = ?"
					+ ",target_date = ? WHERE id = ?";

			pStmt = con.prepareStatement(updateSQL);

			pStmt.setString(1,processName);
			pStmt.setInt(2, projectId);
			pStmt.setString(3, scheduledStartDate);
			pStmt.setString(4, targetDate);
			pStmt.setInt(5, processId);

			pStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public ArrayList<Integer> findAllProcessIdForProjectDeleteByProjectId(int projectId) {
		ArrayList<Integer> processIdList = new ArrayList<Integer>();
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBManager.getConnection();

			String selectSQL = "SELECT id FROM process_info WHERE project_id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1,projectId);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int processId = rs.getInt("id");
				Integer processID = new Integer(processId);

				processIdList.add(processID);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return processIdList;
	}

	public void processDeleteByProjectId(int projectId) {
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBManager.getConnection();

			// projectIdのprojectNameを取得するSELECT文
			String deleteSQL = "DELETE FROM process_info WHERE project_id = ?";

			pStmt = con.prepareStatement(deleteSQL);

			pStmt.setInt(1,projectId);

			pStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String findProcessNameByProcessId(String processId) {
		int intProcessId = Integer.parseInt(processId);

		Connection con = null;
		PreparedStatement pStmt = null;
		String processName = null;

		try {
			con = DBManager.getConnection();

			// projectIdのprojectNameを取得するSELECT文
			String selectSQL = "SELECT process_name FROM process_info WHERE id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1,intProcessId);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				processName = rs.getString("process_name");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return processName;
	}

	public void deleteProcess(int processId) {
		Connection con = null;
		PreparedStatement pStmt = null;
		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();
		try {
			con = DBManager.getConnection();

			// プロジェクトdeleteSQL
			String deleteSQL = "DELETE FROM process_info WHERE id = ?";

			pStmt = con.prepareStatement(deleteSQL);

			pStmt.setInt(1, processId);

			pStmt.execute();

			taskInfoDAO.taskDeleteByOneProcessId(processId);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
