package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserInfoBeans;
import model.ProjectAxisProcessListLogic;

public class UserInfoDAO {

	//新規登録 新規登録入力時にユーザー名=パスワードとする
	public int userInsert(String userName,String mailAddress,String password) {
		Connection con = null;
		PreparedStatement stmt = null;
		int result = 0;
		try {
			con = DBManager.getConnection();
			// 新規登録SQL
			String insertSQL = "INSERT INTO user_info VALUES(0,?,?,?,now());";
			stmt = con.prepareStatement(insertSQL);
			stmt.setString(1, userName);
			stmt.setString(2, mailAddress);
			//パスワードを暗号化
			String encryptedPass = passEncrypt(password);
			stmt.setString(3, encryptedPass);
			result = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public Integer findUserIdByLoginInfo(String mailAddress, String password) {
		Connection con = null;
		try {
			con = DBManager.getConnection();

			String selectSQL = "SELECT * FROM user_info WHERE mail_address = ? and password = ?";

			PreparedStatement pStmt = con.prepareStatement(selectSQL);
			pStmt.setString(1, mailAddress);
			//パスワードを暗号化
			String encryptedPass = passEncrypt(password);
			pStmt.setString(2, encryptedPass);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int intUserId = rs.getInt("id");
			Integer userId = new Integer(intUserId);
			return userId;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public String passEncrypt(String password) {
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";
		//ハッシュ生成処理
		byte[] bytes=null;

		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		return result;
	}

	public UserInfoBeans findUserInfoByUserId(Integer userId) {
		Connection con = null;
		try {
			con = DBManager.getConnection();
			PreparedStatement pStmt = null;
			int intUserId = userId.intValue();

			String selectSQL = "SELECT * FROM user_info WHERE id = ?";

			pStmt = con.prepareStatement(selectSQL);
			pStmt.setInt(1, intUserId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			String userName = rs.getString("user_name");
			String mailAddres = rs.getString("mail_address");

			UserInfoBeans userInfo = new UserInfoBeans(userName,mailAddres);
			return userInfo;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void updateUserInfoNoPassword(String userName, String mailAddress,Integer userId) {

		Connection con = null;
		PreparedStatement pStmt = null;
		int intUserId = userId.intValue();

		try {
			con = DBManager.getConnection();

			String updateSQL = "UPDATE user_info SET user_name = ?,mail_address = ? WHERE id = ?";

			pStmt = con.prepareStatement(updateSQL);
			pStmt.setString(1, userName);
			pStmt.setString(2, mailAddress);
			pStmt.setInt(3, intUserId);
			pStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
	}

	public void updateUserInfo(String userName, String mailAddress, String password, Integer userId) {
		Connection con = null;
		try {
			con = DBManager.getConnection();
			PreparedStatement pStmt = null;
			int intUserId = userId.intValue();

			String updateSQL = "UPDATE user_info SET user_name = ?,mail_address = ?,password = ? WHERE id = ?";

			pStmt = con.prepareStatement(updateSQL);
			pStmt.setString(1, userName);
			pStmt.setString(2, mailAddress);
			//パスワードを暗号化
			String encryptedPass = passEncrypt(password);
			pStmt.setString(3, encryptedPass);
			pStmt.setInt(4, intUserId);
			pStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
	}

	public List<UserInfoBeans> findAll() {
		Connection con = null;
		List<UserInfoBeans> userList = new ArrayList<UserInfoBeans>();
		ProjectAxisProcessListLogic PAPLLogic = new ProjectAxisProcessListLogic();
		PreparedStatement pStmt = null;

		try {
			con = DBManager.getConnection();

			String selectSQL = "SELECT * FROM user_info WHERE id!=1 ";

			pStmt = con.prepareStatement(selectSQL);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String mailAddress = rs.getString("mail_address");
				String userName = rs.getString("user_name");
				Date createDateD = rs.getDate("create_date");
				String createDate = PAPLLogic.ymdSDF(createDateD);
				UserInfoBeans userInfo = new UserInfoBeans(id, userName, mailAddress, createDate);

				userList.add(userInfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public List<UserInfoBeans> search(String searchWord) {
		Connection con = null;
		List<UserInfoBeans> searchedUserList = new ArrayList<UserInfoBeans>();
		ProjectAxisProcessListLogic PAPLLogic = new ProjectAxisProcessListLogic();
		PreparedStatement pStmt = null;

		try {
			con = DBManager.getConnection();

			String selectSQL = "SELECT * FROM user_info WHERE id!=1 AND user_name LIKE ? ";

			pStmt = con.prepareStatement(selectSQL);
			pStmt.setString(1, "%"+searchWord+"%");
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String mailAddress = rs.getString("mail_address");
				String userName = rs.getString("user_name");
				Date createDateD = rs.getDate("create_date");
				String createDate = PAPLLogic.ymdSDF(createDateD);
				UserInfoBeans userInfo = new UserInfoBeans(id, userName, mailAddress, createDate);

				searchedUserList.add(userInfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return searchedUserList;
	}

	public void userDelete(String userIdForUM) {
		Connection con = null;
		PreparedStatement stmt = null;
		int userId = Integer.parseInt(userIdForUM);
		ProjectInfoDAO projectInfoDAO = new ProjectInfoDAO();
		ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();
		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();
		try {
			con = DBManager.getConnection();

			String deleteSQL = "DELETE FROM user_info WHERE id = ?";

			stmt = con.prepareStatement(deleteSQL);

			stmt.setInt(1, userId);
			stmt.execute();

			ArrayList<Integer> projectIdList = projectInfoDAO.findAllProjectIdForUserDeleteByUserId(userId);
			ArrayList<Integer> processIdList = null;
			projectInfoDAO.projectDeleteByUserId(userId);
			if(projectIdList != null) {
				processIdList = processInfoDAO.findAllProcessIdForUserDeleteByProjectId(projectIdList);
				processInfoDAO.processDeleteByProjectId(projectIdList);
			}
			if(processIdList != null) {
				taskInfoDAO.taskDeleteByProcessId(processIdList);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
