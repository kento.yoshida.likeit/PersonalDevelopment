package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import base.DBManager;
import beans.TaskInfoBeans;
import model.ProjectAxisProcessListLogic;

public class TaskInfoDAO {

	public List<TaskInfoBeans> findDateAxisTaskList(Integer userId) {

		List<TaskInfoBeans> dateAxisTaskList = new ArrayList<TaskInfoBeans>();
		Connection con = null;
		PreparedStatement pStmt = null;
		int intUserId = userId.intValue();

		try {
			con = DBManager.getConnection();

			// 今日のdateAxisTaskListを取得するSELECT文
			String selectSQL = "select ti.id AS task_id,ti.task_name,ti.estimated_time AS task_estimated_time,"
					+"ti.actual_time AS task_actual_time,ti.start_time AS task_start_time,"
					+"ti.closing_time AS task_closing_time,ti.action_date AS task_action_date,"
					+"ti.run_status AS task_run_status,pi.process_name,pri.project_name from task_info ti"
					+" inner join process_info pi on ti.process_id = pi.id inner join project_info pri on"
					+" pi.project_id = pri.id inner join user_info ui on pri.user_id = ui.id"
					+" where ui.id = ? AND ti.action_date = CURDATE()";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, intUserId);

			ResultSet rs = pStmt.executeQuery();
			// 結果表に格納されたレコードの内容をtaskInfoインスタンスに設定し、List<TaskInfoBeans> dateAxisTaskList(ArrayList<TaskInfoBeans>)に追加
			while (rs.next()) {
				int id = rs.getInt("task_id");
				String taskName = rs.getString("task_name");
				int estimatedTime = rs.getInt("task_estimated_time");
				int actualTime = rs.getInt("task_actual_time");
				Timestamp tsStartTime = rs.getTimestamp("task_start_time");
				Timestamp tsClosingTime = rs.getTimestamp("task_closing_time");
				String actionDate = rs.getString("task_action_date");
				int runStatus = rs.getInt("task_run_status");
				String processName = rs.getString("process_name");
				String projectName = rs.getString("project_name");

				//Timestamp型変数をHH:mmへ変換
				SimpleDateFormat hhmmSDF = new SimpleDateFormat("HH:mm");

				String startTime;
				if(tsStartTime==null) {
					startTime = "";
				}else {
					startTime = hhmmSDF.format(tsStartTime);
				}

				String closingTime;
				if(tsClosingTime==null) {
					closingTime = "";
				}else {
					closingTime = hhmmSDF.format(tsClosingTime);
				}

				TaskInfoBeans taskInfo = new TaskInfoBeans(id,taskName,estimatedTime,actualTime,startTime,closingTime,
						actionDate,runStatus,processName,projectName);

				dateAxisTaskList.add(taskInfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return dateAxisTaskList;
	}

	public List<TaskInfoBeans> findDateAxisTaskListByPost(Integer userId,String screenDate) {

		List<TaskInfoBeans> dateAxisTaskListByPost = new ArrayList<TaskInfoBeans>();
		Connection con = null;
		PreparedStatement pStmt = null;
		int intUserId = userId.intValue();

		try {
			con = DBManager.getConnection();

			// 今日のdateAxisTaskListを取得するSELECT文
			String selectSQL = "select ti.id AS task_id,ti.task_name,ti.estimated_time AS task_estimated_time,"
					+"ti.actual_time AS task_actual_time,ti.start_time AS task_start_time,"
					+"ti.closing_time AS task_closing_time,ti.action_date AS task_action_date,"
					+"ti.run_status AS task_run_status,pi.process_name,pri.project_name from task_info ti"
					+" inner join process_info pi on ti.process_id = pi.id inner join project_info pri on"
					+" pi.project_id = pri.id inner join user_info ui on pri.user_id = ui.id"
					+" where ui.id = ? AND ti.action_date = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, intUserId);
			pStmt.setString(2, screenDate);

			ResultSet rs = pStmt.executeQuery();
			// 結果表に格納されたレコードの内容をtaskInfoインスタンスに設定し、List<TaskInfoBeans> dateAxisTaskList(ArrayList<TaskInfoBeans>)に追加
			while (rs.next()) {
				int id = rs.getInt("task_id");
				String taskName = rs.getString("task_name");
				int estimatedTime = rs.getInt("task_estimated_time");
				int actualTime = rs.getInt("task_actual_time");
				Timestamp tsStartTime = rs.getTimestamp("task_start_time");
				Timestamp tsClosingTime = rs.getTimestamp("task_closing_time");
				String actionDate = rs.getString("task_action_date");
				int runStatus = rs.getInt("task_run_status");
				String processName = rs.getString("process_name");
				String projectName = rs.getString("project_name");

				//Timestamp型変数をHH:mmへ変換
				SimpleDateFormat hhmmSDF = new SimpleDateFormat("HH:mm");

				String startTime;
				if(tsStartTime==null) {
					startTime = "";
				}else {
					startTime = hhmmSDF.format(tsStartTime);
				}

				String closingTime;
				if(tsClosingTime==null) {
					closingTime = "";
				}else {
					closingTime = hhmmSDF.format(tsClosingTime);
				}

				TaskInfoBeans taskInfo = new TaskInfoBeans(id,taskName,estimatedTime,actualTime,startTime,closingTime,
						actionDate,runStatus,processName,projectName);

				dateAxisTaskListByPost.add(taskInfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return dateAxisTaskListByPost;
	}


	public void goToStatus1(String goToStatus1TaskId) {
		Connection con = null;
		PreparedStatement pStmt = null;
		int taskId = Integer.parseInt(goToStatus1TaskId);
		ProcessInfoDAO processInfo = new ProcessInfoDAO();

		try {
			con = DBManager.getConnection();

			//TaskRunStatus 0→1 SQL
			String updateSQL = " UPDATE task_info SET start_time = now(),run_status = 1,action_date = now() WHERE id = ?";

			pStmt = con.prepareStatement(updateSQL);

			pStmt.setInt(1, taskId);

			pStmt.execute();

			int processRunStatus = processInfo.statusCheckGoTo1(taskId);
			if(processRunStatus == 0) {
				processInfo.goToStasus1(taskId);
			}

		}catch(SQLException e){
			e.printStackTrace();
			return;

		}finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;

				}
			}
		}

	}

	public void goToStatus2(String goToStatus2TaskId) {
		Connection con = null;
		PreparedStatement pStmt = null;
		int taskId = Integer.parseInt(goToStatus2TaskId);

		try {
			con = DBManager.getConnection();

			//TaskRunStatus 1→2 SQL
			String updateSQL = " UPDATE task_info SET closing_time = now(),actual_time = "
					+ "TIMESTAMPDIFF(MINUTE, start_time, closing_time),run_status = 2 WHERE id = ?";

			pStmt = con.prepareStatement(updateSQL);

			pStmt.setInt(1, taskId);

			pStmt.execute();

		}catch(SQLException e){
			e.printStackTrace();
			return;

		}finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;

				}
			}
		}

	}

	public List<TaskInfoBeans> sortDATListByRunStatus(Integer userId,int sentRunStatus,String screenDate){

		List<TaskInfoBeans> sortDateAxisTaskList = new ArrayList<TaskInfoBeans>();
		Connection con = null;
		PreparedStatement pStmt = null;
		int intUserId = userId.intValue();

		try {
			con = DBManager.getConnection();

			// 今日のdateAxisTaskListを取得するSELECT文
			String selectSQL = "select ti.id AS task_id,ti.task_name,ti.estimated_time AS task_estimated_time,"
					+"ti.actual_time AS task_actual_time,ti.start_time AS task_start_time,"
					+"ti.closing_time AS task_closing_time,ti.action_date AS task_action_date,"
					+"ti.run_status AS task_run_status,pi.process_name,pri.project_name from task_info ti"
					+" inner join process_info pi on ti.process_id = pi.id inner join project_info pri on"
					+" pi.project_id = pri.id inner join user_info ui on pri.user_id = ui.id"
					+" where ui.id = ? AND ti.run_status = ? AND ti.action_date = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, intUserId);
			pStmt.setInt(2, sentRunStatus);
			pStmt.setString(3, screenDate);

			ResultSet rs = pStmt.executeQuery();
			// 結果表に格納されたレコードの内容をtaskInfoインスタンスに設定し、List<TaskInfoBeans> dateAxisTaskList(ArrayList<TaskInfoBeans>)に追加
			while (rs.next()) {
				int id = rs.getInt("task_id");
				String taskName = rs.getString("task_name");
				int estimatedTime = rs.getInt("task_estimated_time");
				int actualTime = rs.getInt("task_actual_time");
				Timestamp tsStartTime = rs.getTimestamp("task_start_time");
				Timestamp tsClosingTime = rs.getTimestamp("task_closing_time");
				String actionDate = rs.getString("task_action_date");
				int runStatus = rs.getInt("task_run_status");
				String processName = rs.getString("process_name");
				String projectName = rs.getString("project_name");

				//Timestamp型変数をHH:mmへ変換
				SimpleDateFormat hhmmSDF = new SimpleDateFormat("HH:mm");

				String startTime;
				if(tsStartTime==null) {
					startTime = "";
				}else {
					startTime = hhmmSDF.format(tsStartTime);
				}

				String closingTime;
				if(tsClosingTime==null) {
					closingTime = "";
				}else {
					closingTime = hhmmSDF.format(tsClosingTime);
				}

				TaskInfoBeans taskInfo = new TaskInfoBeans(id,taskName,estimatedTime,actualTime,startTime,closingTime,
						actionDate,runStatus,processName,projectName);

				sortDateAxisTaskList.add(taskInfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return sortDateAxisTaskList;
	}

	public TaskInfoBeans findTaskSettingInfoByTaskId(String taskId){

		Connection con = null;
		PreparedStatement pStmt = null;
		int intTaskId = Integer.parseInt(taskId);

		try {
			con = DBManager.getConnection();

			//TaskInfoを取得するSQL
			String selectSQL = "SELECT ti.task_name,ti.process_id,pri.project_name,pi.process_name,ti.estimated_time,"
					+ "ti.action_date from task_info ti inner join process_info pi on ti.process_id = pi.id inner join "
					+ "project_info pri on pi.project_id = pri.id WHERE ti.id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, intTaskId);

			ResultSet rs = pStmt.executeQuery();

			TaskInfoBeans taskInfo = new TaskInfoBeans();
			if(rs.next()) {
				String taskName = rs.getString("task_name");
				String projectName = rs.getString("project_name");
				String processName = rs.getString("process_name");
				int eatimatedTime = rs.getInt("estimated_time");
				String actionDate = rs.getString("action_date");
				int processId = rs.getInt("process_id");

				taskInfo.setTaskName(taskName);
				taskInfo.setProjectName(projectName);
				taskInfo.setProcessName(processName);
				taskInfo.setEstimatedTime(eatimatedTime);
				taskInfo.setActionDate(actionDate);
				taskInfo.setProcessId(processId);
			}
			return taskInfo;

		}catch(SQLException e){
			e.printStackTrace();
			return null;

		}finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
	}

	public int sumTotalTaskEstimatedTimeByProcessId(int processId) {

		Connection con = null;
		PreparedStatement pStmt = null;
		int totalTaskEstimatedMinTime = 0;

		try {
			con = DBManager.getConnection();

			// processIdに紐づいているtaskのestimatedTimeの合計を取得
			String selectSQL = "SELECT SUM(estimated_time) from task_info WHERE process_id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, processId);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				totalTaskEstimatedMinTime = rs.getInt("SUM(estimated_time)");

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return totalTaskEstimatedMinTime;

	}

	public int countTotalTaskNum(int processId) {

		Connection con = null;
		PreparedStatement pStmt = null;
		int countTotalTaskNum = 0;

		try {
			con = DBManager.getConnection();

			// processIdに紐づいているtask数の合計を取得
			String selectSQL = "SELECT COUNT(id) from task_info WHERE process_id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, processId);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				countTotalTaskNum = rs.getInt("COUNT(id)");

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return countTotalTaskNum;
	}

	public boolean checkAllTaskStatusByProcessId(String processIdSTR) {

		Connection con = null;
		PreparedStatement pStmt = null;
		int processId = Integer.parseInt(processIdSTR);
		boolean allTaskStatusIs2 = false;

		try {
			con = DBManager.getConnection();

			// processIdに紐づいているrunStatusが2でないtask数の合計を取得
			String selectSQL = "SELECT COUNT(id) from task_info WHERE run_status != 2 AND process_id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, processId);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int notStarus2TaskNum = rs.getInt("COUNT(id)");
				if(notStarus2TaskNum == 0) {
					allTaskStatusIs2 = true;
				}else {
					allTaskStatusIs2 = false;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return allTaskStatusIs2;
	}

	public int sumTotalTaskActualTimeByProcessId(String processIdSTR) {

		Connection con = null;
		PreparedStatement pStmt = null;
		int totalTaskActualMinTime = 0;
		int processId = Integer.parseInt(processIdSTR);

		try {
			con = DBManager.getConnection();

			// processIdに紐づいているtaskのactualTimeの合計を取得
			String selectSQL = "SELECT SUM(actual_time) from task_info WHERE process_id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, processId);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				totalTaskActualMinTime = rs.getInt("SUM(actual_time)");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return totalTaskActualMinTime;
	}

	public Timestamp getFinalDoneTaskTimestampByProcessId(String processIdSTR) {
		Connection con = null;
		PreparedStatement pStmt = null;
		Timestamp finalDoneTaskTimestamp = null;
		int processId = Integer.parseInt(processIdSTR);

		try {
			con = DBManager.getConnection();

			// processIdに紐づいたタスクの中の、closingTimeが最も直近であるタスクの、closingTimeの日付を取得
			// closing_timeの降順の一番上のclosing_time
			String selectSQL = "SELECT closing_time from task_info WHERE process_id = ? order by closing_time desc";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, processId);

			ResultSet rs = pStmt.executeQuery();

			//一番上のみ取得
			rs.next();
			finalDoneTaskTimestamp = rs.getTimestamp("closing_time");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return finalDoneTaskTimestamp;

	}

	public List<TaskInfoBeans> findProcessAxisTaskList(String processIdSTR) {
		List<TaskInfoBeans> processAxisTaskList = new ArrayList<TaskInfoBeans>();
		Connection con = null;
		PreparedStatement pStmt = null;
		int processId = Integer.parseInt(processIdSTR);
		ProjectAxisProcessListLogic PAPLLogic = new ProjectAxisProcessListLogic();

		try {
			con = DBManager.getConnection();

			// 今日のdateAxisTaskListを取得するSELECT文
			String selectSQL = "select ti.id AS task_id,ti.task_name,ti.estimated_time AS task_estimated_time,"
					+ "ti.actual_time AS task_actual_time,ti.start_time AS task_start_time,ti.closing_time AS "
					+ "task_closing_time,ti.action_date AS task_action_date,ti.run_status AS task_run_status,"
					+ "pi.process_name,pri.project_name from task_info ti inner join process_info pi on ti.process_id "
					+ "= pi.id inner join project_info pri on pi.project_id = pri.id inner join user_info ui on "
					+ "pri.user_id = ui.id where pi.id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, processId);

			ResultSet rs = pStmt.executeQuery();
			// 結果表に格納されたレコードの内容をtaskInfoインスタンスに設定し、List<TaskInfoBeans> processAxisTaskList(ArrayList<TaskInfoBeans>)に追加
			while (rs.next()) {
				int id = rs.getInt("task_id");
				String taskName = rs.getString("task_name");
				int estimatedTime = rs.getInt("task_estimated_time");
				int actualTime = rs.getInt("task_actual_time");
				Timestamp tsStartTime = rs.getTimestamp("task_start_time");
				Timestamp tsClosingTime = rs.getTimestamp("task_closing_time");
				Date actionDateD = rs.getDate("task_action_date");
				String actionDate = PAPLLogic.ymdSDF(actionDateD);
				int runStatus = rs.getInt("task_run_status");
				String processName = rs.getString("process_name");
				String projectName = rs.getString("project_name");

				//Timestamp型変数をHH:mmへ変換
				SimpleDateFormat hhmmSDF = new SimpleDateFormat("HH:mm");

				String startTime;
				if(tsStartTime==null) {
					startTime = "";
				}else {
					startTime = hhmmSDF.format(tsStartTime);
				}

				String closingTime;
				if(tsClosingTime==null) {
					closingTime = "";
				}else {
					closingTime = hhmmSDF.format(tsClosingTime);
				}

				TaskInfoBeans taskInfo = new TaskInfoBeans(id,taskName,estimatedTime,actualTime,startTime,closingTime,
						actionDate,runStatus,processName,projectName);

				processAxisTaskList.add(taskInfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return processAxisTaskList;
	}

	public List<TaskInfoBeans> sortPATListByRunStatus(String processIdSTR, int sentRunStatus) {

		List<TaskInfoBeans> sortProcessAxisTaskList = new ArrayList<TaskInfoBeans>();
		Connection con = null;
		PreparedStatement pStmt = null;
		ProjectAxisProcessListLogic PAPLLogic = new ProjectAxisProcessListLogic();
		int processId = Integer.parseInt(processIdSTR);
		try {
			con = DBManager.getConnection();

			//sortPATListを取得するSELECT文
			String selectSQL = "select ti.id AS task_id,ti.task_name,ti.estimated_time AS task_estimated_time,"
					+ "ti.actual_time AS task_actual_time,ti.start_time AS task_start_time,ti.closing_time AS"
					+ " task_closing_time,ti.action_date AS task_action_date,ti.run_status AS task_run_status,"
					+ "pi.process_name,pri.project_name from task_info ti inner join process_info pi on "
					+ "ti.process_id = pi.id inner join project_info pri on pi.project_id = pri.id inner join "
					+ "user_info ui on pri.user_id = ui.id where pi.id = ? AND ti.run_status = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, processId);
			pStmt.setInt(2, sentRunStatus);

			ResultSet rs = pStmt.executeQuery();
			// 結果表に格納されたレコードの内容をtaskInfoインスタンスに設定し、List<TaskInfoBeans> dateAxisTaskList(ArrayList<TaskInfoBeans>)に追加
			while (rs.next()) {
				int id = rs.getInt("task_id");
				String taskName = rs.getString("task_name");
				int estimatedTime = rs.getInt("task_estimated_time");
				int actualTime = rs.getInt("task_actual_time");
				Timestamp tsStartTime = rs.getTimestamp("task_start_time");
				Timestamp tsClosingTime = rs.getTimestamp("task_closing_time");
				Date actionDateD = rs.getDate("task_action_date");
				String actionDate = PAPLLogic.ymdSDF(actionDateD);
				int runStatus = rs.getInt("task_run_status");
				String processName = rs.getString("process_name");
				String projectName = rs.getString("project_name");

				//Timestamp型変数をHH:mmへ変換
				SimpleDateFormat hhmmSDF = new SimpleDateFormat("HH:mm");

				String startTime;
				if(tsStartTime==null) {
					startTime = "";
				}else {
					startTime = hhmmSDF.format(tsStartTime);
				}

				String closingTime;
				if(tsClosingTime==null) {
					closingTime = "";
				}else {
					closingTime = hhmmSDF.format(tsClosingTime);
				}

				TaskInfoBeans taskInfo = new TaskInfoBeans(id,taskName,estimatedTime,actualTime,startTime,closingTime,
						actionDate,runStatus,processName,projectName);

				sortProcessAxisTaskList.add(taskInfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return sortProcessAxisTaskList;

	}

	public List<TaskInfoBeans> searchPATListBysearchWord(String processIdSTR, String searchWord) {

		List<TaskInfoBeans> searchedProcessAxisTaskList = new ArrayList<TaskInfoBeans>();
		Connection con = null;
		PreparedStatement pStmt = null;
		ProjectAxisProcessListLogic PAPLLogic = new ProjectAxisProcessListLogic();
		int processId = Integer.parseInt(processIdSTR);
		try {
			con = DBManager.getConnection();

			//sortPATListを取得するSELECT文
			String selectSQL = "select ti.id AS task_id,ti.task_name,ti.estimated_time AS task_estimated_time,"
					+ "ti.actual_time AS task_actual_time,ti.start_time AS task_start_time,ti.closing_time AS "
					+ "task_closing_time,ti.action_date AS task_action_date,ti.run_status AS task_run_status, "
					+ "pi.process_name,pri.project_name from task_info ti inner join process_info pi on  "
					+ "ti.process_id = pi.id inner join project_info pri on pi.project_id = pri.id inner join  "
					+ "user_info ui on pri.user_id = ui.id where pi.id = ? AND ti.task_name LIKE ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, processId);
			pStmt.setString(2, "%" + searchWord + "%");

			ResultSet rs = pStmt.executeQuery();
			// 結果表に格納されたレコードの内容をtaskInfoインスタンスに設定し、List<TaskInfoBeans> searchedProcessAxisTaskList(ArrayList<TaskInfoBeans>)に追加
			while (rs.next()) {
				int id = rs.getInt("task_id");
				String taskName = rs.getString("task_name");
				int estimatedTime = rs.getInt("task_estimated_time");
				int actualTime = rs.getInt("task_actual_time");
				Timestamp tsStartTime = rs.getTimestamp("task_start_time");
				Timestamp tsClosingTime = rs.getTimestamp("task_closing_time");
				Date actionDateD = rs.getDate("task_action_date");
				String actionDate = PAPLLogic.ymdSDF(actionDateD);
				int runStatus = rs.getInt("task_run_status");
				String processName = rs.getString("process_name");
				String projectName = rs.getString("project_name");

				//Timestamp型変数をHH:mmへ変換
				SimpleDateFormat hhmmSDF = new SimpleDateFormat("HH:mm");

				String startTime;
				if(tsStartTime==null) {
					startTime = "";
				}else {
					startTime = hhmmSDF.format(tsStartTime);
				}

				String closingTime;
				if(tsClosingTime==null) {
					closingTime = "";
				}else {
					closingTime = hhmmSDF.format(tsClosingTime);
				}

				TaskInfoBeans taskInfo = new TaskInfoBeans(id,taskName,estimatedTime,actualTime,startTime,closingTime,
						actionDate,runStatus,processName,projectName);

				searchedProcessAxisTaskList.add(taskInfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return searchedProcessAxisTaskList;
	}

	public void taskDeleteByProcessId(ArrayList<Integer> processIdList) {
		Connection con = null;
		PreparedStatement pStmt = null;
		for(Integer processID:processIdList) {
			try {
				int processId = processID.intValue();
				con = DBManager.getConnection();

				// projectIdのprojectNameを取得するSELECT文
				String deleteSQL = "delete FROM task_info WHERE process_id = ?";

				pStmt = con.prepareStatement(deleteSQL);

				pStmt.setInt(1,processId);

				pStmt.execute();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public void newTask(String taskName, int processId, int estimatedTime, String actionDate) {
		Connection con = null;
		PreparedStatement pStmt = null;
		try {
			con = DBManager.getConnection();

			// タスク登録SQL
			String insertSQL = "INSERT INTO task_info(task_name,process_id,estimated_time,action_date,run_status) "
					+ "VALUES(?,?,?,?,0)";

			pStmt = con.prepareStatement(insertSQL);

			pStmt.setString(1,taskName);
			pStmt.setInt(2, processId);
			pStmt.setInt(3, estimatedTime);
			pStmt.setString(4, actionDate);

			pStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void updateTask(int taskId, int processId, String taskName, int estimatedTime, String actionDate) {
		Connection con = null;
		PreparedStatement pStmt = null;
		try {
			con = DBManager.getConnection();

			// タスク登録SQL
			String updateSQL = "UPDATE task_info SET task_name = ?,process_id = ?,estimated_time = ?,action_date = ? WHERE id = ?";

			pStmt = con.prepareStatement(updateSQL);

			pStmt.setString(1,taskName);
			pStmt.setInt(2, processId);
			pStmt.setInt(3, estimatedTime);
			pStmt.setString(4, actionDate);
			pStmt.setInt(5, taskId);

			pStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void taskDeleteByOneProcessId(int processId) {
		Connection con = null;
		PreparedStatement pStmt = null;
		try {
			con = DBManager.getConnection();

			// プロジェクトdeleteSQL
			String deleteSQL = "DELETE FROM task_info WHERE process_id = ?";

			pStmt = con.prepareStatement(deleteSQL);

			pStmt.setInt(1, processId);

			pStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void deleteTask(int taskId) {
		Connection con = null;
		PreparedStatement pStmt = null;
		try {
			con = DBManager.getConnection();

			// プロジェクトdeleteSQL
			String deleteSQL = "DELETE FROM task_info WHERE id = ?";

			pStmt = con.prepareStatement(deleteSQL);

			pStmt.setInt(1, taskId);

			pStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
