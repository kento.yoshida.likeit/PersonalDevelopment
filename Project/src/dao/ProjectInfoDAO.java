package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import base.DBManager;
import beans.ProjectInfoBeans;

public class ProjectInfoDAO {

	public List<ProjectInfoBeans> findProjectList(Integer userId) {
		List<ProjectInfoBeans> projectList = new ArrayList<ProjectInfoBeans>();
		Connection con = null;
		PreparedStatement pStmt = null;
		int intUserId = userId.intValue();

		try {
			con = DBManager.getConnection();

			// ProjectListを取得するSELECT文
			String selectSQL = "select pi.id AS project_id,pi.project_name,pi.target_date from project_info pi "
					+"inner join user_info ui on pi.user_id = ui.id where ui.id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, intUserId);

			ResultSet rs = pStmt.executeQuery();
			// 結果表に格納されたレコードの内容をprojectInfoインスタンスに設定し、List<ProjectInfoBeans> ProjectList(ArrayList<ProjectInfoBeans>)に追加
			ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();
			while (rs.next()) {
				int projectId = rs.getInt("project_id");
				String projectName = rs.getString("project_name");
				Date targetDate = rs.getDate("target_Date");
				SimpleDateFormat ymdSDF = new SimpleDateFormat("yyyy年MM月dd日");
				String formatedTargetDate = ymdSDF.format(targetDate);

				int processQuantity = processInfoDAO.processQuantity(projectId);

				ProjectInfoBeans projectInfo = new ProjectInfoBeans(projectId,projectName,formatedTargetDate,processQuantity);

				projectList.add(projectInfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return projectList;
	}

	public List<ProjectInfoBeans> search(Integer userId, String searchWord) {
		List<ProjectInfoBeans> projectList = new ArrayList<ProjectInfoBeans>();
		Connection con = null;
		PreparedStatement pStmt = null;
		int intUserId = userId.intValue();

		try {
			con = DBManager.getConnection();

			// ProjectListを取得するSELECT文
			//空文字で検索すると全検索になる
			String selectSQL = "select pi.id AS project_id,pi.project_name,pi.target_date from project_info pi inner "
					+ "join user_info ui on pi.user_id = ui.id where ui.id = ? AND pi.project_name LIKE ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, intUserId);
			pStmt.setString(2, "%" + searchWord + "%");

			ResultSet rs = pStmt.executeQuery();
			// 結果表に格納されたレコードの内容をprojectInfoインスタンスに設定し、List<ProjectInfoBeans> ProjectList(ArrayList<ProjectInfoBeans>)に追加
			ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();
			while (rs.next()) {
				int projectId = rs.getInt("project_id");
				String projectName = rs.getString("project_name");
				Date targetDate = rs.getDate("target_Date");
				SimpleDateFormat ymdSDF = new SimpleDateFormat("yyyy年MM月dd日");
				String formatedTargetDate = ymdSDF.format(targetDate);

				int processQuantity = processInfoDAO.processQuantity(projectId);

				ProjectInfoBeans projectInfo = new ProjectInfoBeans(projectId,projectName,formatedTargetDate,processQuantity);

				projectList.add(projectInfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return projectList;
	}

	public String findProjectNameByProjectId(String projectId) {

		int intProjectId = Integer.parseInt(projectId);
		Connection con = null;
		PreparedStatement pStmt = null;
		String projectName = null;

		try {
			con = DBManager.getConnection();

			// projectIdのprojectNameを取得するSELECT文
			String selectSQL = "SELECT project_name FROM project_info WHERE id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1,intProjectId);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				projectName = rs.getString("project_name");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return projectName;
	}

	public ArrayList<Integer> findAllProjectIdForUserDeleteByUserId(int userId) {
		ArrayList<Integer> projectIdList = new ArrayList<Integer>();
		Connection con = null;
		PreparedStatement pStmt = null;
		try {
			con = DBManager.getConnection();

			// projectIdのprojectNameを取得するSELECT文
			String selectSQL = "SELECT id FROM project_info WHERE user_id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1,userId);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int projectId = rs.getInt("id");
				Integer projectID = new Integer(projectId);

				projectIdList.add(projectID);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return projectIdList;
	}

	public void projectDeleteByUserId(int userId) {
		Connection con = null;
		PreparedStatement pStmt = null;
		try {
			con = DBManager.getConnection();

			// projectIdのprojectNameを取得するSELECT文
			String deleteSQL = "DELETE FROM project_info WHERE user_id = ?";

			pStmt = con.prepareStatement(deleteSQL);

			pStmt.setInt(1,userId);

			pStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<ProjectInfoBeans> findProjectInfoListForNewProcessByUserId(Integer userId) {
		Connection con = null;
		PreparedStatement pStmt = null;
		int intUserId = userId.intValue();
		List<ProjectInfoBeans> projectInfoList = new ArrayList<ProjectInfoBeans>();
		try {
			con = DBManager.getConnection();

			String selectSQL = "SELECT id,project_name FROM project_info WHERE user_id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, intUserId);

			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				int projectId = rs.getInt("id");
				String projectName = rs.getString("project_name");

				ProjectInfoBeans projectInfo = new ProjectInfoBeans(projectId,projectName);

				projectInfoList.add(projectInfo);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return projectInfoList;
	}

	public ProjectInfoBeans findProjectInfoForNewProcessByProjectId(String projectIdSTR) {
		Connection con = null;
		PreparedStatement pStmt = null;
		int projectId = Integer.parseInt(projectIdSTR);
		ProjectInfoBeans transitionSourceProjectInfo = null;
		try {
			con = DBManager.getConnection();

			String selectSQL = "SELECT id,project_name FROM project_info WHERE id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, projectId);

			ResultSet rs = pStmt.executeQuery();

			rs.next();
			int intProjectId = rs.getInt("id");
			String projectName = rs.getString("project_name");

			transitionSourceProjectInfo = new ProjectInfoBeans(intProjectId,projectName);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return transitionSourceProjectInfo;
	}

	public void newProject(Integer userId, String projectName, String targetDate) {
		Connection con = null;
		PreparedStatement pStmt = null;
		int intUserId = userId.intValue();
		try {
			con = DBManager.getConnection();

			// タスク登録SQL
			String insertSQL = "INSERT INTO project_info VALUES(0,?,?,?)";

			pStmt = con.prepareStatement(insertSQL);
			pStmt.setString(1,projectName);
			pStmt.setInt(2, intUserId);
			pStmt.setString(3, targetDate);

			pStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public ProjectInfoBeans findProjectSettingInfoByProjectId(int projectId) {
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBManager.getConnection();

			//ProjectInfoを取得するSQL
			String selectSQL = "SELECT project_name,target_date from project_info WHERE id = ?";

			pStmt = con.prepareStatement(selectSQL);

			pStmt.setInt(1, projectId);

			ResultSet rs = pStmt.executeQuery();

			ProjectInfoBeans projectInfo = new ProjectInfoBeans();
			if(rs.next()) {
				String projectName = rs.getString("project_name");
				String targetDate = rs.getString("target_date");

				projectInfo.setProjectName(projectName);
				projectInfo.setTargetDate(targetDate);
			}
			return projectInfo;

		}catch(SQLException e){
			e.printStackTrace();
			return null;

		}finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
	}

	public void updateProject(int projectId, String projectName, String targetDate) {
		Connection con = null;
		PreparedStatement pStmt = null;
		try {
			con = DBManager.getConnection();

			// プロジェクトupdateSQL
			String updateSQL = "UPDATE project_info SET project_name = ?,target_date = ? WHERE id = ?";

			pStmt = con.prepareStatement(updateSQL);

			pStmt.setString(1,projectName);
			pStmt.setString(2, targetDate);
			pStmt.setInt(3, projectId);

			pStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void deleteProject(int projectId) {
		Connection con = null;
		PreparedStatement pStmt = null;
		ProcessInfoDAO processInfoDAO = new ProcessInfoDAO();
		TaskInfoDAO taskInfoDAO = new TaskInfoDAO();
		try {
			con = DBManager.getConnection();

			// プロジェクトdeleteSQL
			String deleteSQL = "DELETE FROM project_info WHERE id = ?";

			pStmt = con.prepareStatement(deleteSQL);

			pStmt.setInt(1, projectId);

			pStmt.execute();

			ArrayList<Integer> processIdList = processInfoDAO.findAllProcessIdForProjectDeleteByProjectId(projectId);
			processInfoDAO.processDeleteByProjectId(projectId);
			if(processIdList != null) {
				taskInfoDAO.taskDeleteByProcessId(processIdList);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
