package beans;

public class ProcessInfoBeans {

	private int id;
	private String processName;
	private int projectId;
	private String estimatedTime;
	private String actualTime;
	private String scheduledStartDate;
	private String startDate;
	private String targetDate;
	private String completionDate;
	private int runStatus;
	private String projectName;
	private int totalTaskNum;

	public ProcessInfoBeans(int id, String processName, int projectId, String estimatedTime, String actualTime,
			String scheduledStartDate, String startDate, String targetDate, String completionDate, int runStatus,
			String projectName, int totalTaskNum) {
		this.id = id;
		this.processName = processName;
		this.projectId = projectId;
		this.estimatedTime = estimatedTime;
		this.actualTime = actualTime;
		this.scheduledStartDate = scheduledStartDate;
		this.startDate = startDate;
		this.targetDate = targetDate;
		this.completionDate = completionDate;
		this.runStatus = runStatus;
		this.projectName = projectName;
		this.totalTaskNum = totalTaskNum;
	}

	public ProcessInfoBeans(int id, int projectId,String processName, String projectName) {
		this.id = id;
		this.projectId = projectId;
		this.processName = processName;
		this.projectName = projectName;

	}

	public ProcessInfoBeans() {

	}

	public ProcessInfoBeans(int id, String processName, String projectName) {
		this.id = id;
		this.processName = processName;
		this.projectName = projectName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getEstimatedTime() {
		return estimatedTime;
	}

	public void setEstimatedTime(String estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	public String getActualTime() {
		return actualTime;
	}

	public void setActualTime(String actualTime) {
		this.actualTime = actualTime;
	}

	public String getScheduledStartDate() {
		return scheduledStartDate;
	}

	public void setScheduledStartDate(String scheduledStartDate) {
		this.scheduledStartDate = scheduledStartDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}

	public String getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(String completionDate) {
		this.completionDate = completionDate;
	}

	public int getRunStatus() {
		return runStatus;
	}

	public void setRunStatus(int runStatus) {
		this.runStatus = runStatus;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public int getTotalTaskNum() {
		return totalTaskNum;
	}

	public void setTotalTaskNum(int totalTaskNum) {
		this.totalTaskNum = totalTaskNum;
	}

}
