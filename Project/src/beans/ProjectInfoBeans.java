package beans;

public class ProjectInfoBeans {
	private int projectId;
	private String projectName;
	private int userId;
	private String targetDate;
	private int processQuantity;

	public ProjectInfoBeans(int projectId, String projectName,String targetDate,int processQuantity) {
		this.projectId = projectId;
		this.projectName = projectName;
		this.targetDate = targetDate;
		this.processQuantity = processQuantity;
	}

	public ProjectInfoBeans(int projectId, String projectName) {
		this.projectId = projectId;
		this.projectName = projectName;
	}

	public ProjectInfoBeans() {

	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}

	public int getProcessQuantity() {
		return processQuantity;
	}

	public void setProcessQuantity(int processQuantity) {
		this.processQuantity = processQuantity;
	}

}
