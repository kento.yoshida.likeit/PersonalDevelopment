package beans;

import java.io.Serializable;

public class UserInfoBeans implements Serializable{
	private int id;
	private String userName;
	private String mailAddress;
	private String password;
	private String createDate;

	public UserInfoBeans(String userName, String mailAddress) {
		this.userName = userName;
		this.mailAddress = mailAddress;
	}



	public UserInfoBeans(int id, String userName, String mailAddress, String createDate) {
		this.id = id;
		this.userName = userName;
		this.mailAddress = mailAddress;
		this.createDate = createDate;
	}



	public UserInfoBeans() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

}
