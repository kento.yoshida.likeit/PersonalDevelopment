package beans;

public class TaskInfoBeans {
	private int id;
	private String taskName;
	private int estimatedTime;
	private int actualTime;
	private String startTime;
	private String closingTime;
	private String actionDate;
	private int runStatus;
	private String processName;
	private String projectName;
	private int processId;

	public TaskInfoBeans(int id, String taskName, int estimatedTime, int actualTime, String startTime,
			String closingTime, String actionDate, int runStatus, String processName, String projectName) {
		this.id = id;
		this.taskName = taskName;
		this.estimatedTime = estimatedTime;
		this.actualTime = actualTime;
		this.startTime = startTime;
		this.closingTime = closingTime;
		this.actionDate = actionDate;
		this.runStatus = runStatus;
		this.processName = processName;
		this.projectName = projectName;
	}

	public TaskInfoBeans() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public int getEstimatedTime() {
		return estimatedTime;
	}

	public void setEstimatedTime(int estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	public int getActualTime() {
		return actualTime;
	}

	public void setActualTime(int actualTime) {
		this.actualTime = actualTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getClosingTime() {
		return closingTime;
	}

	public void setClosingTime(String closingTime) {
		this.closingTime = closingTime;
	}

	public String getActionDate() {
		return actionDate;
	}

	public void setActionDate(String actionDate) {
		this.actionDate = actionDate;
	}

	public int getRunStatus() {
		return runStatus;
	}

	public void setRunStatus(int runStatus) {
		this.runStatus = runStatus;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public int getProcessId() {
		return processId;
	}

	public void setProcessId(int processId) {
		this.processId = processId;
	}

}