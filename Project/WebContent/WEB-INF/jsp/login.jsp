<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>Willject-login</title>
<link href="css/login.css" rel="stylesheet">
</head>
<body>
	<header class="header1">
		<h1>
			<a href="InitialServlet">Willject</a>
		</h1>
		<h2>
			<a href="LoginServlet">ログイン</a>
		</h2>
	</header>
	<div class="parent" align="center">
		<div style="color: red;">${errMsg}</div>
		<form action="LoginServlet" method="post">
			<table>
				<tr>
					<td class="text">メールアドレス</td>
					<td><input class="box" type="text" placeholder="  MailAddress"
						name="mailAddress">
						<p></p></td>
				</tr>
				<tr>
					<td class="text">パスワード</td>
					<td><input class="box" type="password"
						placeholder="  Password" name="password"></td>
				</tr>
			</table>
			<input class="regi" type="submit" value="ログインする">
		</form>
		<div class="this">
			<a href="NewUserServlet"> 新規登録はこちら </a>
		</div>
	</div>
</body>
</html>