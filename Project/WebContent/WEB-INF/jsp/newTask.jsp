<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>Willject-newTask</title>
<!-- UIkit CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
<link rel="stylesheet" href="css/newTask.css">
<!-- UIkit JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit-icons.min.js"></script>
</head>
<body>
	<nav class="uk-navbar-container nav" uk-navbar>

		<div class="uk-navbar-left ">

			<ul class="uk-navbar-nav">
				<li class="uk-active"><a href="DateAxisTaskListServlet">Willject</a></li>
			</ul>

		</div>

		<div class="uk-navbar-right">

			<ul class="uk-navbar-nav">
				<li><a href="DateAxisTaskListServlet">Today's Task</a></li>
				<li><a href="ProjectListServlet">Project</a></li>
				<li><a href="SettingServlet">Setting</a></li>
			</ul>

		</div>

	</nav>
	<div class="parent" align="center">
		<form action="NewTaskServlet" method="post">
			<h1>タスク新規登録</h1>
			<div style="color: red;">${errMsg}</div>
			<table>
				<tr>
					<td>タスク名</td>
					<td><input class="box" type="text" name="taskName"></td>
				</tr>
				<c:if test="${transitionSourceProcessInfo != null}">
				<tr>
					<td>工程</td>
					<td><select name="processId" class="box">
					        <option value="${transitionSourceProcessInfo.id}" selected>${transitionSourceProcessInfo.processName} - ${transitionSourceProcessInfo.projectName}</option>
							<c:forEach var="processInfo" items="${processInfoList}">
							<c:if test="${transitionSourceProcessInfo.id != processInfo.id}">
							<option value="${processInfo.id}">${processInfo.processName} - ${processInfo.projectName}</option>
							</c:if>
							</c:forEach>
					</select></td>
				</tr>
				</c:if>
				<c:if test="${transitionSourceProcessInfo == null}">
				<tr>
					<td>工程</td>
					<td><select name="processId" class="box">
					        <c:forEach var="processInfo" items="${processInfoList}">
							<option value="${processInfo.id}">${processInfo.processName} - ${processInfo.projectName}</option>
							</c:forEach>
					</select></td>
				</tr>
				</c:if>
				<tr>
					<td>見積時間(min)</td>
					<td><input class="box" type="number" name="estimatedTime"></td>
				</tr>
				<tr>
					<td>実行日</td>
					<td><input class="box" type="date" name="actionDate" value="${screenDate}"></td>
				</tr>
			</table>
			<input type="hidden" name="transitionSourceServlet" value="${transitionSourceServlet}">
			<input type="hidden" name="projectId" value="${projectId}">
			<input class="regi" type="submit" value="登録">
		</form>
	</div>
</body>
</html>