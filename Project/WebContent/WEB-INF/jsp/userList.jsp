<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>Willject-userList</title>
<!-- UIkit CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
<link rel="stylesheet" href="css/userList.css">
<!-- UIkit JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit-icons.min.js"></script>
</head>
<body>
	<nav class="uk-navbar-container nav" uk-navbar>

		<div class="uk-navbar-left ">

			<ul class="uk-navbar-nav">
				<li class="uk-active"><a href="DateAxisTaskListServlet">Willject</a></li>
			</ul>

		</div>

		<div class="uk-navbar-right">

			<ul class="uk-navbar-nav">
				<li><a href="DateAxisTaskListServlet">Today's Task</a></li>
				<li><a href="ProjectListServlet">Project</a></li>
				<li><a href="SettingServlet">Setting</a></li>
			</ul>

		</div>

	</nav>

	<div class="body" align="center">
		<table class="tableB  uk-table uk-table-divider">
			<tr align="center">
				<td>ユーザー名</td>
				<td>メールアドレス</td>
				<td>登録日</td>
				<td><div class="uk-margin">
						<form class="uk-search uk-search-default" action="UserListServlet"
							method="post">
							<span uk-search-icon></span> <input class="uk-search-input"
								type="search" placeholder="Search..." name="searchWord">
						</form>
					</div></td>
				<td></td>
			</tr>
			<c:forEach var="user" items="${userList}">
				<tr align="center">
					<td>${user.userName}</td>
					<td>${user.mailAddress}</td>
					<td>${user.createDate}</td>
					<td>
						<button class="uk-button uk-button-secondary stt"
							onclick="location.href='UserEditServlet?userIdForUM=${user.id}'">⛭</button>
					</td>
					<td><a href="UserDeleteServlet?userIdForUM=${user.id}"
						class="uk-icon-link" uk-icon="trash"></a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>