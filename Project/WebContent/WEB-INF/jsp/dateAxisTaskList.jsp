<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Willject-dateAxisTaskList</title>
<!-- UIkit CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
<link rel="stylesheet" href="css/dateAxisTaskList.css">
<!-- UIkit JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit-icons.min.js"></script>
</head>
<body>
	<nav class="uk-navbar-container nav" uk-navbar>

		<div class="uk-navbar-left ">

			<ul class="uk-navbar-nav">
				<li class="uk-active"><a href="DateAxisTaskListServlet">Willject</a></li>
			</ul>

		</div>

		<div class="uk-navbar-right">

			<ul class="uk-navbar-nav">
				<li><a href="DateAxisTaskListServlet">Today's Task</a></li>
				<li><a href="ProjectListServlet">Project</a></li>
				<li><a href="SettingServlet">Setting</a></li>
			</ul>

		</div>

	</nav>
	<div class="uk-child-width-1-2@s uk-text-center uk-align-center "
		uk-grid>
		<div>
			<div
				class="uk-box-shadow-xlarge uk-padding uk-position-top-center  box1">
				<div class="header2block " align="center">
					<div class="header2">
						<table class="tableA">
							<tr class="toptr" align="center">
								<td>全て</td>
								<td>消化</td>
								<td>残り</td>
							</tr>
							<tr align="center">
								<td>${allTaskNum}</td>
								<td>${doneTaskNum}</td>
								<td>${remainingTaskNum}</td>
							</tr>
							<tr align="center">
								<td>${allTaskHour}h</td>
								<td>${doneTaskHour}h</td>
								<td>${remainingTaskHour}h</td>
							</tr>
						</table>
						<progress class="allProgress" value="${doneTaskHour}"
							max="${allTaskHour}"></progress>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="body" align="center">
		<table class="tableB  uk-table uk-table-divider">
			<tr align="center">
				<td>
					<button class="uk-button uk-button-primary TAB"
						onclick="location.href='NewTaskServlet?screenDate=${screenDate}&transitionSourceServlet=DateAxisTaskListServlet'">タスクを登録</button>
				</td>

				<td>タスク<br>
					<form action="DateAxisTaskListServlet" method="post">
						<select name="statusName" class="box" onchange="submit(this.form)">
							<option>ステータスでタスクをソート</option>
							<option value="allStatus">全てのステータス</option>
							<option value="notStarted">未着手</option>
							<option value="inProgress">実行中</option>
							<option value="done">完了</option>
						</select> <input type="hidden" name="screenDate" value="${screenDate}">
					</form>
				</td>
				<td>見積</td>
				<td>実績</td>
				<td>開始</td>
				<td>終了</td>
				<td>
					<div class="date">
						<form action="DateAxisTaskListServlet" method="post">
							<label> <input type="date" name="screenDate"
								value="${screenDate}" onchange="submit(this.form)"></input>
							</label>
						</form>
					</div>
				</td>
			</tr>
			<c:forEach var="dateAxisTask" items="${dateAxisTaskList}">
				<tr align="center">
					<c:choose>
						<c:when test="${dateAxisTask.runStatus == 0}">
							<td>
								<form action="DateAxisTaskListServlet" method="post">
									<button class="uk-button uk-button-danger stt" type="submit"
										name="goToStatus1" value="${dateAxisTask.id}">▶</button>
									<input type="hidden" name="screenDate" value="${screenDate}">
								</form>
							</td>
						</c:when>
						<c:when test="${dateAxisTask.runStatus == 1}">
							<td>
								<form action="DateAxisTaskListServlet" method="post">
									<button class="uk-button uk-button-danger stt" type="submit"
										name="goToStatus2" value="${dateAxisTask.id}">▶⇢✓</button>
									<input type="hidden" name="screenDate" value="${screenDate}">
								</form>
							</td>
						</c:when>
						<c:when test="${dateAxisTask.runStatus == 2}">
							<td><button class="uk-button uk-button-danger stt">
									✓</button></td>
						</c:when>
					</c:choose>
					<td>${dateAxisTask.projectName} - ${dateAxisTask.processName}<br>${dateAxisTask.taskName}
					</td>
					<td>${dateAxisTask.estimatedTime}分</td>
					<td>${dateAxisTask.actualTime}分</td>
					<td>${dateAxisTask.startTime}</td>
					<td>${dateAxisTask.closingTime}</td>
					<td>

						<button class="uk-button uk-button-secondary stt"
							onclick="location.href='TaskSettingServlet?taskId=${dateAxisTask.id}&transitionSourceServlet=DateAxisTaskListServlet'">⛭</button>
					</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>