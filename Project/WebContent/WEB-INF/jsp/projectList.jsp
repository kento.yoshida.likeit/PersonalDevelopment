<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>Willject-projectList</title>
<!-- UIkit CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
<link rel="stylesheet" href="css/projectList.css">
<!-- UIkit JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit-icons.min.js"></script>
</head>
<body>
	<nav class="uk-navbar-container nav" uk-navbar>

		<div class="uk-navbar-left ">

			<ul class="uk-navbar-nav">
				<li class="uk-active"><a href="DateAxisTaskListServlet">Willject</a></li>
			</ul>

		</div>

		<div class="uk-navbar-right">

			<ul class="uk-navbar-nav">
				<li><a href="DateAxisTaskListServlet">Today's Task</a></li>
				<li><a href="ProjectListServlet">Project</a></li>
				<li><a href="SettingServlet">Setting</a></li>
			</ul>

		</div>

	</nav>


	<div class="body" align="center">
		<table class="tableB  uk-table uk-table-divider">
			<tr align="center">
				<td><form action="NewProjectServlet" method="get">
						<button class="uk-button uk-button-primary TAB">プロジェクトを登録</button>
					</form></td>
				<td>工程数</td>
				<td>完了予定日</td>
				<td>
					<div class="uk-margin">
						<form class="uk-search uk-search-default" action="ProjectListServlet" method="post">
							<span uk-search-icon></span> <input class="uk-search-input"
								type="search" placeholder="Search..." name="searchWord">
						</form>
					</div>
				</td>
			</tr>
			<c:forEach var="projectInfo" items="${projectInfoList}">
			<tr align="center">
				<td><a href="ProjectAxisProcessListServlet?projectId=${projectInfo.projectId}">${projectInfo.projectName}</a></td>
				<td>${projectInfo.processQuantity}</td>
				<td>${projectInfo.targetDate}</td>
				<td>
						<button class="uk-button uk-button-secondary stt"
						onclick="location.href='ProjectSettingServlet?projectId=${projectInfo.projectId}'">⛭</button>
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>