<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>Willject-taskSetting</title>
<!-- UIkit CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
<link rel="stylesheet" href="css/taskSetting.css">
<!-- UIkit JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit-icons.min.js"></script>
</head>
<body>
	<nav class="uk-navbar-container nav" uk-navbar>

		<div class="uk-navbar-left ">

			<ul class="uk-navbar-nav">
				<li class="uk-active"><a href="DateAxisTaskListServlet">Willject</a></li>
			</ul>

		</div>

		<div class="uk-navbar-right">

			<ul class="uk-navbar-nav">
				<li><a href="DateAxisTaskListServlet">Today's Task</a></li>
				<li><a href="ProjectListServlet">Project</a></li>
				<li><a href="SettingServlet">Setting</a></li>
			</ul>

		</div>

	</nav>
	<div class="parent" align="center">
		<form action="TaskSettingServlet" method="post">
			<h1>
				タスク情報設定 <a href="TaskDeleteServlet?taskId=${taskId}&transitionSourceServlet=${transitionSourceServlet}&processId=${taskInfo.processId}&projectId=${projectId}" class="uk-icon-link"
					uk-icon="trash"></a>
			</h1>
			<div style="color: red;">${errMsg}</div>
			<table>
				<tr>
					<td>タスク名</td>
					<td><input class="box" type="text"
						value="${taskInfo.taskName}" name="taskName"></td>
				</tr>
				<tr>
					<td>工程</td>
					<td><select name="processId" class="box">
							<option value="${taskInfo.processId}" selected>${taskInfo.processName}
								- ${taskInfo.projectName}</option>
							<c:forEach var="processInfo" items="${processInfoList}">
								<c:if test="${taskInfo.processId != processInfo.id}">
									<option value="${processInfo.id}">${processInfo.processName}
										- ${processInfo.projectName}</option>
								</c:if>
							</c:forEach>

					</select></td>
				</tr>
				<tr>
					<td>見積時間(分)</td>
					<td><input class="box" type="number"
						value="${taskInfo.estimatedTime}" name="estimatedTime"></td>
				</tr>
				<tr>
					<td>実行日</td>
					<td><input class="box" type="date"
						value="${taskInfo.actionDate}" name="actionDate" ></td>
				</tr>
			</table>
				<input type="hidden" name="processId" value="${processId}">
				<input type="hidden" name="taskId" value="${taskId}">
				<input type="hidden" name="transitionSourceServlet" value="${transitionSourceServlet}">
				<input type="hidden" name="projectId" value="${projectId}">
				 <input class="regi"
				type="submit" value="更新">
		</form>
	</div>
</body>
</html>