<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>Willject-setting</title>
<!-- UIkit CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
<link rel="stylesheet" href="css/setting.css">
<!-- UIkit JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit-icons.min.js"></script>
</head>
<body>
	<nav class="uk-navbar-container nav" uk-navbar>
		<div class="uk-navbar-left ">
			<ul class="uk-navbar-nav">
				<li class="uk-active"><a href="DateAxisTaskListServlet">Willject</a></li>
			</ul>
		</div>
		<div class="uk-navbar-right">
			<ul class="uk-navbar-nav">
				<li><a href="DateAxisTaskListServlet">Today's Task</a></li>
				<li><a href="ProjectListServlet">Project</a></li>
				<li><a href="SettingServlet">Setting</a></li>
			</ul>
		</div>
	</nav>
	<div class="parent" align="center">
		<h1>Setting</h1>
		<table>
			<tr>
				<td>ユーザー情報編集</td>
				<td><a href="UserEditServlet"
					class="uk-icon-link uk-margin-small-right" uk-icon="user"></a></td>
			</tr>
			<tr>
				<td>ログアウト</td>
				<td><a href="LogoutServlet"
					class="uk-icon-link uk-margin-small-right" uk-icon="sign-out"></a></td>
			</tr>
			<c:if test="${userId == 1}">
				<tr>
					<td>ユーザー一覧</td>
					<td><a href="UserListServlet" class="uk-icon-link"
						uk-icon="users"></a></td>
				</tr>
			</c:if>
		</table>
	</div>
</body>
</html>