<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta charset="UTF-8">
<title>Willject-userEdit</title>
<!-- UIkit CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
<link rel="stylesheet" href="css/userEdit.css">
<!-- UIkit JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit-icons.min.js"></script>
</head>
<body>
	<nav class="uk-navbar-container nav" uk-navbar>

		<div class="uk-navbar-left ">

			<ul class="uk-navbar-nav">
				<li class="uk-active"><a href="DateAxisTaskListServlet">Willject</a></li>
			</ul>

		</div>

		<div class="uk-navbar-right">

			<ul class="uk-navbar-nav">
				<li><a href="DateAxisTaskListServlet">Today's Task</a></li>
				<li><a href="ProjectListServlet">Project</a></li>
				<li><a href="SettingServlet">Setting</a></li>
			</ul>

		</div>

	</nav>
	<div class="parent" align="center">
		<form action="UserEditServlet" method="post">
			<h1>ユーザー情報編集画面</h1>
			<p Style=color:red;>${errMsg}</p>
			<table>
				<tr>
					<td>ユーザーネーム</td>
					<td><input class="box" type="text" name="userName" value="${userInfo.userName}">
					</td>
				</tr>
				<tr>
					<td>メールアドレス</td>
					<td><input class="box" type="text" name="mailAddress" value="${userInfo.mailAddress}">
					</td>
				</tr>
				<tr>
					<td>パスワード</td>
					<td><input class="box" type="password" placeholder="Password" name="password">
					</td>
				</tr>
				<tr>
					<td>パスワード(確認)</td>
					<td><input class="box" type="password" placeholder="Password" name="rePassword">
					</td>
				</tr>
			</table>
			<input class="regi" type="submit" value="送信">
			<input type="hidden" name="userIdForUM" value="${userIdForUM}">
		</form>
	</div>
</body>
</html>