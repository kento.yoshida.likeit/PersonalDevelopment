<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta charset="UTF-8">
<title>Willject-taskDelete</title>
<!-- UIkit CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
<link rel="stylesheet" href="css/userDelete.css">
<!-- UIkit JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit-icons.min.js"></script>
</head>
<body>
	<nav class="uk-navbar-container nav" uk-navbar>

		<div class="uk-navbar-left ">

			<ul class="uk-navbar-nav">
				<li class="uk-active"><a href="DateAxisTaskListServlet">Willject</a></li>
			</ul>

		</div>

		<div class="uk-navbar-right">

			<ul class="uk-navbar-nav">
				<li><a href="DateAxisTaskListServlet">Today's Task</a></li>
				<li><a href="ProjectListServlet">Project</a></li>
				<li><a href="SettingServlet">Setting</a></li>
			</ul>

		</div>

	</nav>
	<div class="parent" align="center">
		<h2>本当に削除して良いですか？</h2>
		<form action="ProjectDeleteServlet" method="post">
		<input type="hidden" name="projectId" value="${projectId}">
			<button class="regi">はい</button>
		</form>
		<form action="ProjectSettingServlet" method="get">
		<input type="hidden" name="projectId" value="${projectId}">
			<button class="regi">いいえ</button>
		</form>
	</div>
</body>
</html>