<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>Willject-processSetting</title>
<!-- UIkit CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
<link rel="stylesheet" href="css/processSetting.css">
<!-- UIkit JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit-icons.min.js"></script>
</head>
<body>
	<nav class="uk-navbar-container nav" uk-navbar>

		<div class="uk-navbar-left ">

			<ul class="uk-navbar-nav">
				<li class="uk-active"><a href="DateAxisTaskListServlet">Willject</a></li>
			</ul>

		</div>

		<div class="uk-navbar-right">

			<ul class="uk-navbar-nav">
				<li><a href="DateAxisTaskListServlet">Today's Task</a></li>
				<li><a href="ProjectListServlet">Project</a></li>
				<li><a href="SettingServlet">Setting</a></li>
			</ul>

		</div>

	</nav>
	<div class="parent" align="center">
		<form action="ProcessSettingServlet" method="post">
			<h1>工程情報設定　<a href="ProcessDeleteServlet?processId=${processId}&projectId=${processInfo.projectId}" class="uk-icon-link"
					uk-icon="trash"></a></h1>
					<div style="color: red;">${errMsg}</div>
			<table>
				<tr>
					<td>工程名</td>
					<td><input class="box" type="text" value="${processInfo.processName}" name="processName"></td>
				</tr>
				<tr>
					<td>プロジェクト</td>
					<td><select name="projectId" class="box">
					<option value="${processInfo.projectId}" selected>${processInfo.projectName}</option>
							<c:forEach var="projectInfo" items="${projectInfoList}">
								<c:if test="${processInfo.projectId != projectInfo.projectId}">
									<option value="${projectInfo.projectId}">${projectInfo.projectName}</option>
								</c:if>
							</c:forEach>
					</select>
				</tr>
				<tr>
					<td>開始予定日</td>
					<td><input class="box" type="date" value="${processInfo.scheduledStartDate}" name="scheduledStartDate"></td>
				</tr>
				<tr>
					<td>完了予定日</td>
					<td><input class="box" type="date" value="${processInfo.targetDate}" name="targetDate"></td>
				</tr>
			</table>
			<input class="regi" type="submit" value="更新">
			<input type="hidden" name="processId" value="${processId}">
		</form>
	</div>
</body>
</html>