<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>Willject-projectAxisProcessList</title>
<!-- UIkit CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
<link rel="stylesheet" href="css/projectAxisProcessList.css">
<!-- UIkit JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit-icons.min.js"></script>
</head>
<body>
	<nav class="uk-navbar-container nav" uk-navbar>

		<div class="uk-navbar-left ">

			<ul class="uk-navbar-nav">
				<li class="uk-active"><a href="DateAxisTaskListServlet">Willject</a></li>
			</ul>

		</div>

		<div class="uk-navbar-right">

			<ul class="uk-navbar-nav">
				<li><a href="DateAxisTaskListServlet">Today's Task</a></li>
				<li><a href="ProjectListServlet">Project</a></li>
				<li><a href="SettingServlet">Setting</a></li>
			</ul>

		</div>

	</nav>
	<div class="uk-child-width-1-2@s uk-text-center uk-align-center "
		uk-grid>
		<div>
			<div
				class="uk-box-shadow-xlarge uk-padding uk-position-top-center  box1">
				<div class="header2block " align="center">
					<div class="header2">
						<p>${projectName}- 全工程</p>
						<table class="tableA">
							<tr class="toptr" align="center">
								<td>全て</td>
								<td>消化</td>
								<td>残り</td>
							</tr>
							<tr align="center">
								<td>${allProcessNum}</td>
								<td>${doneProcessNum}</td>
								<td>${remainingProcessNum}</td>
							</tr>
							<tr align="center">
								<td>${allProcessHour}h</td>
								<td>${doneProcessHour}h</td>
								<td>${remainingProcessHour}h</td>
							</tr>
						</table>
						<progress class="allProgress" value="${doneProcessHour}"
							max="${allProcessHour}"></progress>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="body" align="center">
		<table class="tableB  uk-table uk-table-divider">
			<tr align="center">
				<td>
					<button class="uk-button uk-button-primary TAB"
						onclick="location.href='NewProcessServlet?projectId=${projectId}'">
						<nobr>工程を登録</nobr>
					</button>
				</td>
				<td>工程<br>
					<form action="ProjectAxisProcessListServlet" method="post">
						<select name="statusName" class="box" onchange="submit(this.form)">
							<option>ステータスで工程をソート</option>
							<option value="allStatus">全てのステータス</option>
							<option value="notStarted">未着手</option>
							<option value="inProgress">実行中</option>
							<option value="done">完了</option>
						</select> <input type="hidden" name="projectId" value="${projectId}">
					</form>
				</td>
				<td><nobr>タスク数</nobr></td>
				<td>見積</td>
				<td>実績</td>
				<td>開始予定日</td>
				<td>開始日</td>
				<td>完了予定日</td>
				<td>完了日</td>
				<td><div class="uk-margin">
						<form class="uk-search uk-search-default"
							action="ProjectAxisProcessListServlet" method="post">
							<span uk-search-icon></span> <input class="uk-search-input"
								type="search" placeholder="Search..." name="searchWord">
							<input type="hidden" name="projectId" value="${projectId}">
						</form>
					</div></td>
			</tr>
			<c:forEach var="projectAxisProcess" items="${projectAxisProcessList}">
				<tr align="center">
					<c:choose>
						<c:when test="${projectAxisProcess.runStatus == 0}">
							<td><button class="uk-button uk-button-danger stt">▶</button>
							</td>
						</c:when>
						<c:when test="${projectAxisProcess.runStatus == 1}">
							<td>
								<form action="ProjectAxisProcessListServlet" method="post">
									<button class="uk-button uk-button-danger stt" type="submit"
										name="goToStatus2" value="${projectAxisProcess.id}">▶⇢✓</button>
									<input type="hidden" name="projectId" value="${projectId}">
								</form>
							</td>
						</c:when>
						<c:when test="${projectAxisProcess.runStatus == 2}">
							<td><button class="uk-button uk-button-danger stt">✓</button></td>
						</c:when>
					</c:choose>
					<td><a
						href="ProcessAxisTaskListServlet?processId=${projectAxisProcess.id}&projectId=${projectId}">${projectAxisProcess.processName}</a></td>
					<td>${projectAxisProcess.totalTaskNum}</td>
					<td>${projectAxisProcess.estimatedTime}時間</td>
					<td>${projectAxisProcess.actualTime}時間</td>
					<td>${projectAxisProcess.scheduledStartDate}</td>
					<td>${projectAxisProcess.startDate}</td>
					<td>${projectAxisProcess.targetDate}</td>
					<td>${projectAxisProcess.completionDate}</td>
					<td>
						<button class="uk-button uk-button-secondary stt"
							onclick="location.href='ProcessSettingServlet?processId=${projectAxisProcess.id}'">⛭</button>

					</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>