<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>Willject-processAxisTaskList</title>
<!-- UIkit CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
<link rel="stylesheet" href="css/processAxisTaskList.css">
<!-- UIkit JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit-icons.min.js"></script>
</head>
<body>
	<nav class="uk-navbar-container nav" uk-navbar>

		<div class="uk-navbar-left ">

			<ul class="uk-navbar-nav">
				<li class="uk-active"><a href="DateAxisTaskListServlet">Willject</a></li>
			</ul>

		</div>

		<div class="uk-navbar-right">

			<ul class="uk-navbar-nav">
				<li><a href="DateAxisTaskListServlet">Today's Task</a></li>
				<li><a href="ProjectListServlet">Project</a></li>
				<li><a href="SettingServlet">Setting</a></li>
			</ul>

		</div>

	</nav>
	<div class="uk-child-width-1-2@s uk-text-center uk-align-center "
		uk-grid>
		<div>
			<div
				class="uk-box-shadow-xlarge uk-padding uk-position-top-center  box1">
				<div class="header2block " align="center">
					<p>${projectName} - ${processName}</p>
					<div class="header2">
						<table class="tableA">
							<tr class="toptr" align="center">
								<td>全て</td>
								<td>消化</td>
								<td>残り</td>
							</tr>
							<tr align="center">
								<td>${allTaskNum}</td>
								<td>${doneTaskNum}</td>
								<td>${remainingTaskNum}</td>
							</tr>
							<tr align="center">
								<td>${allTaskHour}h</td>
								<td>${doneTaskHour}h</td>
								<td>${remainingTaskHour}h</td>
							</tr>
						</table>
						<progress class="allProgress" value="${doneTaskHour}"
							max="${allTaskHour}"></progress>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="body" align="center">
		<table class="tableB  uk-table uk-table-divider">
			<tr align="center">
				<td>
					<button class="uk-button uk-button-primary TAB"
						onclick="location.href='NewTaskServlet?processId=${processId}&transitionSourceServlet=ProcessAxisTaskListServlet'">タスクを登録</button>
				</td>
				<td>タスク<br>
					<form action="ProcessAxisTaskListServlet" method="post">
						<select name="statusName" class="box" onchange="submit(this.form)">
					        <option>ステータスでタスクをソート</option>
							<option value="allStatus">全てのステータス</option>
							<option value="notStarted">未着手</option>
							<option value="inProgress">実行中</option>
							<option value="done">完了</option>
						</select><input type="hidden" name="processId" value="${processId}">
						<input type="hidden" name="projectId" value="${projectId}">
					</form>
				</td>
				<td>見積</td>
				<td>実績</td>
				<td>開始</td>
				<td>終了</td>
				<td>実行日</td>
				<td>
					<div class="uk-margin">
						<form class="uk-search uk-search-default"
							action="ProcessAxisTaskListServlet" method="post">
							<span uk-search-icon></span> <input class="uk-search-input"
								type="search" placeholder="Search..." name="searchWord">
							<input type="hidden" name="processId" value="${processId}">
							<input type="hidden" name="projectId" value="${projectId}">
						</form>
					</div>
				</td>
			</tr>
			<c:forEach var="processAxisTask" items="${processAxisTaskList}">
				<tr align="center">
					<c:choose>
						<c:when test="${processAxisTask.runStatus == 0}">
							<td>
								<form action="ProcessAxisTaskListServlet" method="post">
									<button class="uk-button uk-button-danger stt" type="submit"
										name="goToStatus1" value="${processAxisTask.id}">▶</button>
									<input type="hidden" name="processId" value="${processId}">
									<input type="hidden" name="projectId" value="${projectId}">
								</form>
							</td>
						</c:when>
						<c:when test="${processAxisTask.runStatus == 1}">
							<td>
								<form action="ProcessAxisTaskListServlet" method="post">
									<button class="uk-button uk-button-danger stt" type="submit"
										name="goToStatus2" value="${processAxisTask.id}">▶⇢✓</button>
									<input type="hidden" name="processId" value="${processId}">
									<input type="hidden" name="projectId" value="${projectId}">
								</form>
							</td>
						</c:when>
						<c:when test="${processAxisTask.runStatus == 2}">
							<td><button class="uk-button uk-button-danger stt">
									✓</button></td>
						</c:when>
					</c:choose>
					<td>${processAxisTask.taskName}</td>
					<td>${processAxisTask.estimatedTime}分</td>
					<td>${processAxisTask.actualTime}分</td>
					<td>${processAxisTask.startTime}</td>
					<td>${processAxisTask.closingTime}</td>
					<td>${processAxisTask.actionDate}</td>
					<td>
						<button class="uk-button uk-button-secondary stt"
							onclick="location.href='TaskSettingServlet?taskId=${processAxisTask.id}&transitionSourceServlet=ProcessAxisTaskListServlet&projectId=${projectId}&ProcessId=${processId}'">⛭</button>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>