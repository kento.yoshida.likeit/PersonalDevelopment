<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta charset="UTF-8">
<title>Willject-projectSetting</title>
<!-- UIkit CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
<link rel="stylesheet" href="css/projectSetting.css">
<!-- UIkit JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/js/uikit-icons.min.js"></script>
</head>
<body>
	<nav class="uk-navbar-container nav" uk-navbar>

		<div class="uk-navbar-left ">

			<ul class="uk-navbar-nav">
				<li class="uk-active"><a href="DateAxisTaskListServlet">Willject</a></li>
			</ul>

		</div>

		<div class="uk-navbar-right">

			<ul class="uk-navbar-nav">
				<li><a href="DateAxisTaskListServlet">Today's Task</a></li>
				<li><a href="ProjectListServlet">Project</a></li>
				<li><a href="SettingServlet">Setting</a></li>
			</ul>

		</div>

	</nav>
	<div class="parent" align="center">
		<form action="ProjectSettingServlet" method="post">
			<h1>プロジェクト情報設定　<a href="ProjectDeleteServlet?projectId=${projectId}" class="uk-icon-link"
					uk-icon="trash"></a></h1>
					<div style="color: red;">${errMsg}</div>
			<table>
				<tr>
					<td>プロジェクト名</td>
					<td><input class="box" type="text" value="${projectInfo.projectName}" name="projectName"></td>
				</tr>
				<tr>
					<td>完了予定日</td>
					<td><input class="box" type="date" value="${projectInfo.targetDate}" name="targetDate"></td>
				</tr>
			</table>
			<input class="regi" type="submit" value="更新">
			<input type="hidden" name="projectId" value="${projectId}">
		</form>
	</div>
</body>
</html>