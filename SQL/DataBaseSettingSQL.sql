#データベース生成
CREATE DATABASE willject DEFAULT CHARACTER SET utf8;

#テーブル生成
CREATE TABLE user_info(id int PRIMARY KEY AUTO_INCREMENT,user_name varchar(100) NOT NULL,mail_address varchar(100) UNIQUE NOT NULL,
password varchar(100) NOT NULL,create_date date NOT NULL);
CREATE TABLE project_info(id int PRIMARY KEY AUTO_INCREMENT,project_name varchar(100) NOT NULL,user_id int NOT NULL,
target_date date NOT NULL);
CREATE TABLE process_info(id int PRIMARY KEY AUTO_INCREMENT,process_name varchar(100) NOT NULL,project_id int NOT NULL,
estimated_time int NOT NULL,actual_time int NOT NULL,scheduled_start_date date NOT NULL,start_date timestamp NOT NULL,
target_date date NOT NULL,completion_date timestamp NOT NULL, run_status int NOT NULL);
CREATE TABLE task_info(id int PRIMARY KEY AUTO_INCREMENT,task_name varchar(100) NOT NULL,process_id int NOT NULL,
estimated_time int NOT NULL,actual_time int NOT NULL,start_time timestamp NOT NULL,closing_time timestamp NOT NULL,action_date date NOT NULL,run_status int NOT NULL);

#論理名挿入
ALTER TABLE user_info COMMENT 'ユーザー情報';
ALTER TABLE user_info MODIFY mail_address varchar(100) NOT NULL COMMENT 'メールアドレス';
ALTER TABLE user_info MODIFY id int AUTO_INCREMENT COMMENT 'ID';
ALTER TABLE user_info MODIFY user_name varchar(100) NOT NULL COMMENT 'ユーザーネーム';
ALTER TABLE user_info MODIFY password varchar(100) NOT NULL COMMENT 'パスワード';
ALTER TABLE user_info MODIFY create_date date NOT NULL COMMENT '登録日';
ALTER TABLE task_info COMMENT 'タスク情報';
ALTER TABLE task_info MODIFY id int AUTO_INCREMENT COMMENT 'ID';
ALTER TABLE task_info MODIFY task_name varchar(100) NOT NULL COMMENT 'タスク名';
ALTER TABLE task_info MODIFY process_id int NOT NULL COMMENT '工程ID';
ALTER TABLE task_info MODIFY estimated_time int NOT NULL COMMENT '見積時間';
ALTER TABLE task_info MODIFY actual_time int NOT NULL COMMENT '実績時間';
ALTER TABLE task_info MODIFY start_time timestamp NOT NULL COMMENT '開始時間';
ALTER TABLE task_info MODIFY closing_time timestamp NOT NULL COMMENT '終了時間';
ALTER TABLE task_info MODIFY action_date date NOT NULL COMMENT '実行日';
ALTER TABLE task_info MODIFY run_status int NOT NULL COMMENT '実行ステータス';
ALTER TABLE process_info COMMENT '工程情報';
ALTER TABLE process_info MODIFY id int AUTO_INCREMENT COMMENT 'ID';
ALTER TABLE process_info MODIFY process_name varchar(100) NOT NULL COMMENT '工程名';
ALTER TABLE process_info MODIFY project_id int NOT NULL COMMENT 'プロジェクトID';
ALTER TABLE process_info MODIFY estimated_time int NOT NULL COMMENT '見積時間';
ALTER TABLE process_info MODIFY actual_time int NOT NULL COMMENT '実績時間';
ALTER TABLE process_info MODIFY scheduled_start_date date NOT NULL COMMENT '開始予定日';
ALTER TABLE process_info MODIFY start_date timestamp NOT NULL COMMENT '開始日時';
ALTER TABLE process_info MODIFY target_date date NOT NULL COMMENT '完了予定日';
ALTER TABLE process_info MODIFY completion_date timestamp NOT NULL COMMENT '完了日時';
ALTER TABLE process_info MODIFY run_status int NOT NULL COMMENT '実行ステータス';
ALTER TABLE project_info COMMENT 'プロジェクト情報';
ALTER TABLE project_info MODIFY id int AUTO_INCREMENT COMMENT 'ID';
ALTER TABLE project_info MODIFY project_name varchar(100) NOT NULL COMMENT 'プロジェクト名';
ALTER TABLE project_info MODIFY user_id int NOT NULL COMMENT 'ユーザーID';
ALTER TABLE project_info MODIFY target_date date NOT NULL COMMENT '完了予定日';

#DEFAULTNULLを設定
ALTER TABLE task_info MODIFY COLUMN actual_time int COMMENT '実績時間';
ALTER TABLE task_info ALTER COLUMN actual_time SET DEFAULT NULL;
ALTER TABLE task_info MODIFY COLUMN start_time timestamp NULL DEFAULT NULL COMMENT '開始時間';
ALTER TABLE task_info MODIFY COLUMN closing_time timestamp NULL DEFAULT NULL COMMENT '終了時間';
ALTER TABLE process_info MODIFY COLUMN actual_time int COMMENT '実績時間';
ALTER TABLE process_info ALTER COLUMN actual_time SET DEFAULT NULL;
ALTER TABLE process_info MODIFY COLUMN start_date timestamp NULL DEFAULT NULL COMMENT '開始日時';
ALTER TABLE process_info MODIFY COLUMN completion_date timestamp NULL DEFAULT NULL COMMENT '完了日時';